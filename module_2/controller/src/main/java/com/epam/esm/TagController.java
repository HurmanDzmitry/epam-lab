package com.epam.esm;

import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tags")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping
    public ResponseEntity<TagDto> add(@RequestBody TagDto request) {
        return ResponseEntity.ok(tagService.add(request));
    }

    @GetMapping
    public ResponseEntity<List<TagDto>> getAll() {
        return ResponseEntity.ok(tagService.getAll());
    }

    @GetMapping("/{name}")
    public ResponseEntity<TagDto> getById(@PathVariable String name) {
        return ResponseEntity.ok(tagService.getById(name));
    }

    @DeleteMapping("/{name}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable String name) {
        tagService.deleteById(name);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
