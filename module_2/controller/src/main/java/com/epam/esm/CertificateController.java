package com.epam.esm;

import com.epam.esm.certificate.CertificateService;
import com.epam.esm.dto.certificate.CertificateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/certificates")
public class CertificateController {

    private final CertificateService certificateService;

    @Autowired
    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @PostMapping
    public ResponseEntity<CertificateDto> add(@RequestBody CertificateDto request) {
        return ResponseEntity.ok(certificateService.add(request));
    }

    @GetMapping
    public ResponseEntity<List<CertificateDto>> getAll() {
        return ResponseEntity.ok(certificateService.getAll());
    }

    @GetMapping("/{name}")
    public ResponseEntity<CertificateDto> getById(@PathVariable String name) {
        return ResponseEntity.ok(certificateService.getById(name));
    }

    @PutMapping
    public ResponseEntity<CertificateDto> update(@RequestBody CertificateDto request) {
        return ResponseEntity.ok(certificateService.update(request));
    }

    @DeleteMapping("/{name}")
    public ResponseEntity<HttpStatus> deleteById(@PathVariable String name) {
        certificateService.deleteById(name);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/search")
    public ResponseEntity<List<CertificateDto>> search(@RequestParam String searchWhat, @RequestParam String searchBy) {
        return ResponseEntity.ok(certificateService.search(searchWhat, searchBy));
    }

    @GetMapping("/sort")
    public ResponseEntity<List<CertificateDto>> sort(@RequestParam String sortBy, @RequestParam String sortHow) {
        return ResponseEntity.ok(certificateService.sort(sortBy, sortHow));
    }
}