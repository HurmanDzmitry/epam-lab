package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class DeleteEntitityException extends RuntimeException {

    private static final long serialVersionUID = 5902796660301138534L;

    public DeleteEntitityException(String message) {
        super(message);
    }

    public DeleteEntitityException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeleteEntitityException(Throwable cause) {
        super(cause);
    }
}

