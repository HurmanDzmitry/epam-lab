package com.epam.esm;

import java.util.List;

public interface ApiService<T, K> {

    T add(T dtoEntity);

    List<T> getAll();

    T getById(K id);

    void deleteById(K id);
}
