package com.epam.esm.tag;

import com.epam.esm.certificate_tag.CertificateTagDao;
import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.dto.tag.TagDtoMapper;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.CreationEntitityException;
import com.epam.esm.exception.DeleteEntitityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private final TagDao tagDao;
    private final CertificateTagDao certificateTagDao;
    private final TagDtoMapper tagDtoMapper;

    @Autowired
    public TagServiceImpl(TagDao tagDao, CertificateTagDao certificateTagDao, TagDtoMapper tagDtoMapper) {
        this.tagDao = tagDao;
        this.certificateTagDao = certificateTagDao;
        this.tagDtoMapper = tagDtoMapper;
    }

    @Transactional
    @Override
    public TagDto add(@Valid @NotNull TagDto dtoEntity) {
        Tag tag = tagDao.findById(dtoEntity.getName());
        if (tag != null) {
            throw new CreationEntitityException("This entity has already been created.");
        }
        tag = tagDao.create(tagDtoMapper.toEntity(dtoEntity));
        return tagDtoMapper.toDto(tag);
    }

    @Override
    public List<TagDto> getAll() {
        return tagDao.findAll()
                .stream()
                .map(tagDtoMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public TagDto getById(@NotBlank String id) {
        return tagDtoMapper.toDto(tagDao.findById(id));
    }

    @Transactional
    @Override
    public void deleteById(@NotBlank String id) {
        if (tagDao.findById(id) == null) {
            throw new DeleteEntitityException("There is no entity with this identifier. There is nothing to delete.");
        }
        certificateTagDao.delete(id);
        tagDao.delete(id);
    }
}
