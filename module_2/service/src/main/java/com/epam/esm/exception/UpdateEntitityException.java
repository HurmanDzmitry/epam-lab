package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class UpdateEntitityException extends RuntimeException {

    private static final long serialVersionUID = -203581376345928302L;

    public UpdateEntitityException(String message) {
        super(message);
    }

    public UpdateEntitityException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateEntitityException(Throwable cause) {
        super(cause);
    }
}

