package com.epam.esm.command.search.impl;

import com.epam.esm.certificate.CertificateDao;
import com.epam.esm.certificate_tag.CertificateTagDao;
import com.epam.esm.command.search.SearchCommand;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.dto.certificate.CertificateDtoMapper;

import java.util.List;
import java.util.stream.Collectors;

public class TagNameSearchCommand implements SearchCommand<CertificateDto> {

    @Override
    public List<CertificateDto> execute(String searchWhat, CertificateDtoMapper certificateDtoMapper,
                                        CertificateTagDao certificateTagDao, CertificateDao certificateDao) {
        return certificateTagDao.findAllCertificateForTag(searchWhat)
                .stream()
                .map(i -> certificateDao.findById(i.getCertificateName()))
                .map(i -> certificateDtoMapper.toDto(i, certificateTagDao.findAllTagsForCertificate(i.getName())))
                .collect(Collectors.toList());
    }
}
