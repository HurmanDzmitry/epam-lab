package com.epam.esm.command.search;

import com.epam.esm.certificate.CertificateDao;
import com.epam.esm.certificate_tag.CertificateTagDao;
import com.epam.esm.dto.certificate.CertificateDtoMapper;

import java.util.List;

public interface SearchCommand<E> {

    List<E> execute(String searchWhat, CertificateDtoMapper certificateDtoMapper,
                    CertificateTagDao certificateTagDao, CertificateDao certificateDao);
}
