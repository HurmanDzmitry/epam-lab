package com.epam.esm.command.search;

import com.epam.esm.command.search.impl.DescriptionSearchCommand;
import com.epam.esm.command.search.impl.NameSearchCommand;
import com.epam.esm.command.search.impl.TagNameSearchCommand;
import com.epam.esm.dto.certificate.CertificateDto;

import java.util.HashMap;
import java.util.Map;

public class SearchCommandProvider {

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String TAG_NAME = "tagName";

    private final Map<String, SearchCommand<CertificateDto>> commands = new HashMap<>();

    {
        commands.put(NAME, new NameSearchCommand());
        commands.put(DESCRIPTION, new DescriptionSearchCommand());
        commands.put(TAG_NAME, new TagNameSearchCommand());
    }

    public Map<String, SearchCommand<CertificateDto>> getCommands() {
        return commands;
    }
}