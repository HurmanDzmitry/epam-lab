package com.epam.esm.certificate;

import com.epam.esm.ApiService;
import com.epam.esm.dto.certificate.CertificateDto;

import java.util.List;

public interface CertificateService extends ApiService<CertificateDto, String> {

    CertificateDto update(CertificateDto dtoEntity);

    List<CertificateDto> getByTagName(String tagName);

    List<CertificateDto> search(String searchWhat, String searchBy);

    List<CertificateDto> sort(String sortBy, String sortHow);
}
