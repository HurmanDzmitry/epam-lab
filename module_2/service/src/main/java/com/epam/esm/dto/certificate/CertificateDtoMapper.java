package com.epam.esm.dto.certificate;

import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.CertificateTag;
import com.epam.esm.entity.Tag;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class CertificateDtoMapper {

    private final ModelMapper mapper;

    @Autowired
    public CertificateDtoMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Certificate toEntity(CertificateDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, Certificate.class);
    }

    public CertificateDto toDto(Certificate entity, List<CertificateTag> certificateTags) {
        CertificateDto certificateDto = Objects.isNull(entity) ? null : mapper.map(entity, CertificateDto.class);
        Set<Tag> tagSet = certificateTags.stream().map(i -> new Tag(i.getTagName())).collect(Collectors.toSet());
        certificateDto.setTags(tagSet);
        return certificateDto;
    }
}
