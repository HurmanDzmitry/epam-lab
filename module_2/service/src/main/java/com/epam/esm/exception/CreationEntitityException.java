package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class CreationEntitityException extends RuntimeException {

    private static final long serialVersionUID = -5797198753454326041L;

    public CreationEntitityException(String message) {
        super(message);
    }

    public CreationEntitityException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreationEntitityException(Throwable cause) {
        super(cause);
    }
}

