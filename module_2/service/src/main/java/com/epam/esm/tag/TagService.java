package com.epam.esm.tag;

import com.epam.esm.ApiService;
import com.epam.esm.dto.tag.TagDto;

public interface TagService extends ApiService<TagDto, String> {
}
