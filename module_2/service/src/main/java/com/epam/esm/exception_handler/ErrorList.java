package com.epam.esm.exception_handler;

public enum ErrorList {

    CREATION_ENTITITY_RESPONSE(1005, "This entity has already been created."),
    UPDATE_ENTITITY_RESPONSE(1006, "This entity has not yet been created."),
    DELETE_ENTITITY_RESPONSE(1007, "There is no entity with this identifier. " +
            "There is nothing to delete."),
    NOT_FOUND_RESPONSE(1000, "The requested resource is not found."),
    NULL_POINTER_RESPONSE(1001, "Null pointer.");

    private int code;
    private String message;

    ErrorList(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
