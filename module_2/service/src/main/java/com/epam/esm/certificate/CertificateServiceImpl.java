package com.epam.esm.certificate;

import com.epam.esm.certificate_tag.CertificateTagDao;
import com.epam.esm.command.search.SearchCommand;
import com.epam.esm.command.search.SearchCommandProvider;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.dto.certificate.CertificateDtoMapper;
import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.CertificateTag;
import com.epam.esm.entity.Tag;
import com.epam.esm.exception.CreationEntitityException;
import com.epam.esm.exception.DeleteEntitityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CertificateServiceImpl implements CertificateService {

    private final CertificateDao certificateDao;
    private final CertificateTagDao certificateTagDao;
    private final CertificateDtoMapper certificateDtoMapper;

    @Autowired
    public CertificateServiceImpl(CertificateDao certificateDao, CertificateTagDao certificateTagDao,
                                  CertificateDtoMapper certificateDtoMapper) {
        this.certificateDao = certificateDao;
        this.certificateTagDao = certificateTagDao;
        this.certificateDtoMapper = certificateDtoMapper;
    }

    @Transactional
    @Override
    public CertificateDto update(@Valid @NotNull CertificateDto dtoEntity) {
        if (certificateDao.findById(dtoEntity.getName()) == null) {
            throw new CreationEntitityException("This entity has not yet been created.");
        }
        Certificate certificate = certificateDtoMapper.toEntity(dtoEntity);
        certificate.setLastUpdateDate(OffsetDateTime.now());
        certificateDao.update(certificate);
        String certificateName = certificate.getName();
        certificateTagDao.delete(certificateName);
        Set<Tag> tags = dtoEntity.getTags();
        tags.forEach(i -> certificateTagDao.create(new CertificateTag(certificateName, i.getName())));
        dtoEntity.setLastUpdateDate(certificate.getLastUpdateDate().toString());
        return dtoEntity;
    }

    @Override
    public List<CertificateDto> getByTagName(@Valid @NotBlank String tagName) {
        List<CertificateTag> allTagsForCertificate = certificateTagDao.findAllTagsForCertificate(tagName);
        return allTagsForCertificate
                .stream()
                .map(i -> certificateDao.findById(i.getCertificateName()))
                .map(i -> certificateDtoMapper.toDto(i, certificateTagDao.findAllTagsForCertificate(i.getName())))
                .collect(Collectors.toList());
    }

    @Override
    public List<CertificateDto> search(@NotBlank String searchWhat, @NotBlank String searchBy) {
        SearchCommand<CertificateDto> searchCommand = new SearchCommandProvider().getCommands().get(searchBy);
        return searchCommand.execute(searchWhat, certificateDtoMapper, certificateTagDao, certificateDao);
    }

    @Override
    public List<CertificateDto> sort(@NotBlank String sortBy, @NotBlank String sortHow) {
        return certificateDao.sort(sortBy, sortHow)
                .stream()
                .map(i -> certificateDtoMapper.toDto(i, certificateTagDao.findAllTagsForCertificate(i.getName())))
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public CertificateDto add(@Valid @NotNull CertificateDto dtoEntity) {
        Certificate certificate = certificateDtoMapper.toEntity(dtoEntity);
        if (certificateDao.findById(certificate.getName()) != null) {
            throw new CreationEntitityException("This entity has already been created.");
        }
        certificate.setCreateDate(OffsetDateTime.now());
        certificate.setLastUpdateDate(OffsetDateTime.now());
        certificateDao.create(certificate);
        dtoEntity.getTags()
                .forEach(i -> certificateTagDao.create(new CertificateTag(certificate.getName(), i.getName())));
        dtoEntity.setLastUpdateDate(certificate.getLastUpdateDate().toString());
        return dtoEntity;
    }

    @Override
    public List<CertificateDto> getAll() {
        return certificateDao.findAll()
                .stream()
                .map(i -> certificateDtoMapper.toDto(i, certificateTagDao.findAllTagsForCertificate(i.getName())))
                .collect(Collectors.toList());
    }

    @Override
    public CertificateDto getById(@NotBlank String id) {
        Certificate certificate = certificateDao.findById(id);
        if (certificate == null) {
            return null;
        }
        List<CertificateTag> certificateTags = certificateTagDao.findAllTagsForCertificate(certificate.getName());
        return certificateDtoMapper.toDto(certificate, certificateTags);
    }

    @Transactional
    @Override
    public void deleteById(@NotBlank String id) {
        if (certificateDao.findById(id) == null) {
            throw new DeleteEntitityException("There is no entity with this identifier. There is nothing to delete.");
        }
        certificateTagDao.delete(id);
        certificateDao.delete(id);
    }
}
