package com.epam.esm.exception_handler;

import com.epam.esm.exception.CreationEntitityException;
import com.epam.esm.exception.DeleteEntitityException;
import com.epam.esm.exception.UpdateEntitityException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CreationEntitityException.class)
    public ResponseEntity<ErrorMessage> handleCreationEntityException(CreationEntitityException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorList.CREATION_ENTITITY_RESPONSE.getCode(), ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UpdateEntitityException.class)
    public ResponseEntity<ErrorMessage> handleUpdateEntityException(UpdateEntitityException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorList.UPDATE_ENTITITY_RESPONSE.getCode(), ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DeleteEntitityException.class)
    public ResponseEntity<ErrorMessage> handleDeletenEntityException(DeleteEntitityException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorList.DELETE_ENTITITY_RESPONSE.getCode(), ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}