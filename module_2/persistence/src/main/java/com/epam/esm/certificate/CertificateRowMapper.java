package com.epam.esm.certificate;

import com.epam.esm.entity.Certificate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;

@Component
public class CertificateRowMapper implements RowMapper<Certificate> {

    private static final String NAME = "name";
    private static final String DESCRIPTION = "description";
    private static final String PRICE = "price";
    private static final String CREATE_DATE = "create_date";
    private static final String LAST_UPDATE_DATE = "last_update_date";
    private static final String DURATION = "duration";

    @Override
    public Certificate mapRow(ResultSet rs, int rowNum) throws SQLException {
        Certificate certificate = new Certificate();

        certificate.setName(rs.getString(NAME));
        certificate.setDescription(rs.getString(DESCRIPTION));
        certificate.setPrice(BigDecimal.valueOf(rs.getDouble(PRICE)));
        certificate.setCreateDate(OffsetDateTime.parse(rs.getString(CREATE_DATE)));
        certificate.setLastUpdateDate(OffsetDateTime.parse(rs.getString(LAST_UPDATE_DATE)));
        certificate.setDuration(rs.getInt(DURATION));

        return certificate;
    }
}