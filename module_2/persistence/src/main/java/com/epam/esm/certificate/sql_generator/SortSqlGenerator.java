package com.epam.esm.certificate.sql_generator;

public class SortSqlGenerator {

    private static final String BLANK = "SELECT * FROM certificate ORDER BY ";

    public String generate(String sortBy, String sortHow) {
        StringBuilder sb = new StringBuilder(BLANK);
        sortBy = sortBy.replaceAll(" ", ", ");
        sb.append(sortBy);
        sb.append(" ");
        sb.append(sortHow.toUpperCase());
        sb.append(";");
        return sb.toString();
    }
}
