package com.epam.esm.certificate_tag;

import com.epam.esm.entity.CertificateTag;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CertificateTagRowMapper implements RowMapper<CertificateTag> {

    private static final String NAME_CERTIFICATE = "name_certificate";
    private static final String NAME_TAG = "name_tag";

    @Override
    public CertificateTag mapRow(ResultSet rs, int rowNum) throws SQLException {
        CertificateTag certificateTag = new CertificateTag();

        certificateTag.setCertificateName(rs.getString(NAME_CERTIFICATE));
        certificateTag.setTagName(rs.getString(NAME_TAG));

        return certificateTag;
    }
}