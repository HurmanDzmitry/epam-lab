package com.epam.esm;

import java.util.List;

public interface ApiDao<T, K> {

    T create(T entity);

    List<T> findAll();

    T findById(K id);

    void delete(K id);
}