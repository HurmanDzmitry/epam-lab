package com.epam.esm.certificate;

import com.epam.esm.ApiDao;
import com.epam.esm.entity.Certificate;

import java.util.List;

public interface CertificateDao extends ApiDao<Certificate, String> {

    Certificate update(Certificate entity);

    List<Certificate> searchByName(String name);

    List<Certificate> searchByDescription(String description);

    List<Certificate> sort(String sortBy, String sortHow);
}
