package com.epam.esm.certificate;

import com.epam.esm.certificate.sql_generator.SortSqlGenerator;
import com.epam.esm.entity.Certificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CertificateDaoImpl implements CertificateDao {

    private static final String CREATE = "INSERT INTO certificate (name, description, price, " +
            "create_date, last_update_date, duration) VALUES (?, ?, ?, ?, ?, ?);";
    private static final String FIND_ALL = "SELECT * FROM certificate;";
    private static final String FIND_BY_ID = "SELECT * FROM certificate WHERE name = ?;";
    private static final String UPDATE = "UPDATE certificate SET description = ?, price = ?, " +
            "last_update_date = ?, duration = ? WHERE name = ?;";
    private static final String DELETE = "DELETE FROM certificate WHERE name = ?;";
    private static final String SEARCH_BY_NAME = "SELECT * FROM certificate WHERE name LIKE ?;";
    private static final String SEARCH_BY_DESCRIPTION = "SELECT * FROM certificate WHERE description LIKE ?;";

    private static final char AUXILIARY_CHARACTER = '%';

    private final JdbcTemplate jdbcTemplate;
    private final CertificateRowMapper certificateRowMapper;

    @Autowired
    public CertificateDaoImpl(JdbcTemplate jdbcTemplate, CertificateRowMapper certificateRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.certificateRowMapper = certificateRowMapper;
    }

    @Override
    public Certificate create(Certificate entity) {
        jdbcTemplate.update(CREATE,
                entity.getName(),
                entity.getDescription(),
                entity.getPrice(),
                entity.getCreateDate().toString(),
                entity.getLastUpdateDate().toString(),
                entity.getDuration());
        return entity;
    }

    @Override
    public List<Certificate> findAll() {
        try {
            return jdbcTemplate.query(FIND_ALL, certificateRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Certificate findById(String id) {
        try {
            return jdbcTemplate.queryForObject(FIND_BY_ID, new Object[]{id}, certificateRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void delete(String id) {
        jdbcTemplate.update(DELETE, id);
    }

    @Override
    public Certificate update(Certificate entity) {
        jdbcTemplate.update(UPDATE,
                entity.getDescription(),
                entity.getPrice(),
                entity.getLastUpdateDate().toString(),
                entity.getDuration(),
                entity.getName());
        return entity;
    }

    @Override
    public List<Certificate> searchByName(String name) {
        try {
            return jdbcTemplate.query(SEARCH_BY_NAME,
                    new Object[]{AUXILIARY_CHARACTER + name + AUXILIARY_CHARACTER}, certificateRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Certificate> searchByDescription(String description) {
        try {
            return jdbcTemplate.query(SEARCH_BY_DESCRIPTION,
                    new Object[]{AUXILIARY_CHARACTER + description + AUXILIARY_CHARACTER}, certificateRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<Certificate> sort(String sortBy, String sortHow) {
        try {
            return jdbcTemplate.query(new SortSqlGenerator().generate(sortBy, sortHow), certificateRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }
}