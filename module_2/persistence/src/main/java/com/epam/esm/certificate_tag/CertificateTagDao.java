package com.epam.esm.certificate_tag;

import com.epam.esm.ApiDao;
import com.epam.esm.entity.CertificateTag;

import java.util.List;

public interface CertificateTagDao extends ApiDao<CertificateTag, String> {

    CertificateTag update(CertificateTag entity);

    List<CertificateTag> findAllTagsForCertificate(String id);

    List<CertificateTag> findAllCertificateForTag(String id);
}
