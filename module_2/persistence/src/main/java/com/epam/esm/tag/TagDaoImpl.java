package com.epam.esm.tag;

import com.epam.esm.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TagDaoImpl implements TagDao {

    private static final String CREATE = "INSERT INTO tag (name) VALUES (?);";
    private static final String FIND_ALL = "SELECT * FROM tag;";
    private static final String FIND_BY_ID = "SELECT * FROM tag WHERE name = ?;";
    private static final String DELETE = "DELETE FROM tag WHERE name = ?;";

    private final JdbcTemplate jdbcTemplate;
    private final TagRowMapper tagRowMapper;

    @Autowired
    public TagDaoImpl(JdbcTemplate jdbcTemplate, TagRowMapper tagRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.tagRowMapper = tagRowMapper;
    }

    @Override
    public Tag create(Tag entity) {
        jdbcTemplate.update(CREATE, entity.getName());
        return entity;
    }

    @Override
    public List<Tag> findAll() {
        try {
            return jdbcTemplate.query(FIND_ALL, tagRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Tag findById(String id) {
        try {
            return jdbcTemplate.queryForObject(FIND_BY_ID, new Object[]{id}, tagRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void delete(String id) {
        jdbcTemplate.update(DELETE, id);
    }
}
