package com.epam.esm.tag;

import com.epam.esm.ApiDao;
import com.epam.esm.entity.Tag;

public interface TagDao extends ApiDao<Tag, String> {
}