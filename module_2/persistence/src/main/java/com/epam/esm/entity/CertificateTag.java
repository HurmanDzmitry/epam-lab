package com.epam.esm.entity;

import java.io.Serializable;
import java.util.Objects;

public class CertificateTag implements Serializable {

    private static final long serialVersionUID = 2314416477341140145L;

    private String certificateName;
    private String tagName;

    public CertificateTag() {
    }

    public CertificateTag(String certificateName, String tagName) {
        this.certificateName = certificateName;
        this.tagName = tagName;
    }

    public String getCertificateName() {
        return certificateName;
    }

    public void setCertificateName(String certificateName) {
        this.certificateName = certificateName;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CertificateTag that = (CertificateTag) o;
        return Objects.equals(certificateName, that.certificateName) &&
                Objects.equals(tagName, that.tagName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(certificateName, tagName);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "certificateName='" + certificateName + '\'' +
                ", tagName='" + tagName + '\'' +
                '}';
    }
}
