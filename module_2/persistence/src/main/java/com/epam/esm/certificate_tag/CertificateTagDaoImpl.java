package com.epam.esm.certificate_tag;

import com.epam.esm.entity.CertificateTag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CertificateTagDaoImpl implements CertificateTagDao {

    private static final String CREATE = "INSERT INTO certificates_has_tags (name_certificate, name_tag) " +
            "VALUES (?, ?);";
    private static final String FIND_ALL = "SELECT * FROM certificates_has_tags;";
    private static final String FIND_BY_NAME_CERTIFICATE = "SELECT * FROM certificates_has_tags " +
            "WHERE name_certificate = ?;";
    private static final String FIND_FISRT_BY_NAME_CERTIFICATE = "SELECT * FROM certificates_has_tags " +
            "WHERE name_certificate = ? LIMIT 1;";
    private static final String FIND_BY_NAME_TAG = "SELECT * FROM certificates_has_tags " +
            "WHERE name_tag = ?;";
    private static final String UPDATE = "UPDATE certificates_has_tags SET name_tag = ? " +
            "WHERE name_certificate = ?;";
    private static final String DELETE = "DELETE FROM certificates_has_tags " +
            "WHERE name_certificate = ?;";

    private final JdbcTemplate jdbcTemplate;
    private final CertificateTagRowMapper certificateTagRowMapper;

    @Autowired
    public CertificateTagDaoImpl(JdbcTemplate jdbcTemplate, CertificateTagRowMapper certificateTagRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.certificateTagRowMapper = certificateTagRowMapper;
    }

    @Override
    public CertificateTag create(CertificateTag entity) {
        jdbcTemplate.update(CREATE,
                entity.getCertificateName(),
                entity.getTagName());
        return entity;
    }

    @Override
    public List<CertificateTag> findAll() {
        try {
            return jdbcTemplate.query(FIND_ALL, certificateTagRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public CertificateTag findById(String id) {
        try {
            return jdbcTemplate.queryForObject(FIND_FISRT_BY_NAME_CERTIFICATE,
                    new Object[]{id}, certificateTagRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public CertificateTag update(CertificateTag entity) {
        jdbcTemplate.update(UPDATE,
                entity.getTagName(),
                entity.getCertificateName());
        return entity;
    }

    @Override
    public List<CertificateTag> findAllTagsForCertificate(String id) {
        try {
            return jdbcTemplate.query(FIND_BY_NAME_CERTIFICATE, new Object[]{id}, certificateTagRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public List<CertificateTag> findAllCertificateForTag(String id) {
        try {
            return jdbcTemplate.query(FIND_BY_NAME_TAG, new Object[]{id}, certificateTagRowMapper);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void delete(String id) {
        jdbcTemplate.update(DELETE, id);
    }
}
