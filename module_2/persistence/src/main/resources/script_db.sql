CREATE TABLE tag
(
  name VARCHAR(100) PRIMARY KEY
);


CREATE TABLE certificate
(
  name             VARCHAR(200) PRIMARY KEY,
  description      VARCHAR(1000) NOT NULL,
  price            DOUBLE        NOT NULL,
  create_date      VARCHAR(50)   NOT NULL,
  last_update_date VARCHAR(50)   NOT NULL,
  duration         INT           NOT NULL
);


CREATE TABLE certificates_has_tags
(
  name_tag         VARCHAR(100) NOT NULL,
  name_certificate VARCHAR(200) NOT NULL,
  FOREIGN KEY (name_tag) REFERENCES tag (name),
  FOREIGN KEY (name_certificate) REFERENCES certificate (name)
);


INSERT INTO tag
VALUES ('#beauty'),
       ('#entertainment'),
       ('#training'),
       ('#photoshoot'),
       ('#sport'),
       ('#quests'),
       ('#food'),
       ('#travels');


INSERT INTO certificate
VALUES ('Online confectionery master class', 'Online confectionery master class', 100,
        '2020-08-01T23:59:59.999-07:00', '2020-08-20T23:59:59.999+01:00', 50),
       ('SPA rituals in Hammam', 'SPA rituals in Hammam', 200,
        '2020-08-02T23:59:59.999-06:00', '2020-08-21T23:59:59.999+02:00', 40),
       ('Acting courses', 'Acting courses', 300,
        '2020-08-03T23:59:59.999-05:00', '2020-08-22T23:59:59.999+03:00', 30),
       ('Swimming with a dolphin', 'Swimming with a dolphin', 400,
        '2020-08-04T23:59:59.999-04:00', '2020-08-23T23:59:59.999+04:00', 20),
       ('Course "Basics of drawing"', 'Course "Basics of drawing"', 500,
        '2020-08-05T23:59:59.999-03:00', '2020-08-24T23:59:59.999+05:00', 60),
       ('Aroma care "Hot cherry"', 'Aroma care "Hot cherry"', 600,
        '2020-08-06T23:59:59.999-02:00', '2020-08-25T23:59:59.999+06:00', 70),
       ('Two-level go-kart track', 'Two-level go-kart track', 700,
        '2020-08-07T23:59:59.999-01:00', '2020-08-26T23:59:59.999+07:00', 80),
       ('Massage session', 'Massage session', 800,
        '2020-08-08T23:59:59.999+00:00', '2020-08-27T23:59:59.999+08:00', 90),
       ('Tea ceremony', 'Tea ceremony', 900,
        '2020-08-09T23:59:59.999+01:00', '2020-08-28T23:59:59.999+09:00', 100);


INSERT INTO certificates_has_tags
VALUES ('#food', 'Online confectionery master class'),
       ('#training', 'Online confectionery master class'),
       ('#beauty', 'SPA rituals in Hammam'),
       ('#training', 'Acting courses'),
       ('#travels', 'Swimming with a dolphin'),
       ('#entertainment', 'Swimming with a dolphin'),
       ('#training', 'Course "Basics of drawing"'),
       ('#beauty', 'Aroma care "Hot cherry"'),
       ('#entertainment', 'Two-level go-kart track'),
       ('#sport', 'Two-level go-kart track'),
       ('#beauty', 'Massage session'),
       ('#food', 'Tea ceremony'),
       ('#training', 'Tea ceremony'),
       ('#entertainment', 'Tea ceremony');