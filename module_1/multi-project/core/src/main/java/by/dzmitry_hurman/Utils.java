package by.dzmitry_hurman;

public class Utils {

    public static boolean isAllPositiveNumbers(String... str) {
        for (String s : str) {
            if (!Utils.isAllPositiveNumbers(s)) {
                return false;
            }
        }
        return true;
    }
}
