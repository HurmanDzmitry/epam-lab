package com.epam.esm.exception_handler;

public enum ErrorResponse {

    INVALID_CREDENTIALS_RESPONSE(1003, "invalid.credential.default"),
    CREATION_ENTITY_RESPONSE(1005, "creation.error.default"),
    UPDATE_ENTITY_RESPONSE(1006, "update.error.default"),
    EMPTY_RESULT_RESPONSE(1007, "empty.result.error.default"),
    VALIDATION_ENTITY_RESPONSE(1008, "invalid.entity.error.default"),
    VALIDATION_PARAM_RESPONSE(1009, "invalid.param.error.default"),
    DELETE_ENTITY_RESPONSE(1010, "deletion.error.default");

    private int code;
    private String message;

    ErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
