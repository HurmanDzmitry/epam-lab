package com.epam.esm.impl;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.dto.partial_certificate.PartialCertificateDto;
import com.epam.esm.impl.certificate.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.groups.Default;
import java.util.Map;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value = "/certificates")
@Validated
public class CertificateController {

    private final CertificateService certificateService;

    @Autowired
    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @PostMapping
    public ResponseEntity<CertificateDto> save(@Validated({Saving.class, Default.class})
                                               @NotNull @RequestBody CertificateDto request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(tagLink(certificateService.save(request)));
    }

    @PutMapping
    public ResponseEntity<CertificateDto> update(@Validated({Updating.class, Default.class})
                                                 @NotNull @RequestBody CertificateDto request) {
        return ResponseEntity.ok(tagLink(certificateService.update(request)));
    }

    @PatchMapping
    public ResponseEntity<CertificateDto> partialUpdate(@Validated @NotNull @RequestBody PartialCertificateDto request) {
        return ResponseEntity.ok(tagLink(certificateService.partialUpdate(request)));
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<CertificateDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        EntityPageDto<CertificateDto> entityPageDtos = certificateService.findAll(params);
        entityPageDtos.getEntities().forEach(this::tagLink);
        return ResponseEntity.ok(entityPageDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CertificateDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(tagLink(certificateService.findById(id)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteById(@NotNull @Positive @PathVariable Integer id) {
        certificateService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    private CertificateDto tagLink(CertificateDto certificate) {
        certificate.getTags().forEach(tag -> certificate.add(linkTo(TagController.class)
                .slash(tag.getId()).withRel("tags")));
        return certificate;
    }
}