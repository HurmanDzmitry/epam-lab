package com.epam.esm.config.security;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static com.epam.esm.config.security.Constants.*;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilter jwtFilter;

    @Autowired
    public SecurityConfig(JwtFilter jwtFilter) {
        this.jwtFilter = jwtFilter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)

                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, CERTIFICATES).permitAll()
                .antMatchers(HttpMethod.GET, CERTIFICATES_ID).permitAll()
                .antMatchers(HttpMethod.GET, TAGS).permitAll()
                .antMatchers(HttpMethod.GET, TAGS_ID).permitAll()
                .antMatchers(HttpMethod.POST, USERS).permitAll()
                .antMatchers(HttpMethod.POST, USERS_SIGNIN).permitAll()

                .antMatchers(HttpMethod.POST, PURCHASES).authenticated()
                .antMatchers(HttpMethod.GET, PURCHASES_ID).authenticated()
                .antMatchers(HttpMethod.GET, TAGS_PURCHASES_USERS_ID).authenticated()
                .antMatchers(HttpMethod.PUT, USERS).authenticated()
                .antMatchers(HttpMethod.GET, USERS_ID).authenticated()

                .antMatchers(HttpMethod.POST, CERTIFICATES).hasRole(ADMIN)
                .antMatchers(HttpMethod.PUT, CERTIFICATES).hasRole(ADMIN)
                .antMatchers(HttpMethod.DELETE, CERTIFICATES_ID).hasRole(ADMIN)
                .antMatchers(HttpMethod.PATCH, CERTIFICATES).hasRole(ADMIN)
                .antMatchers(HttpMethod.PUT, PURCHASES).hasRole(ADMIN)
                .antMatchers(HttpMethod.GET, PURCHASES).hasRole(ADMIN)
                .antMatchers(HttpMethod.DELETE, PURCHASES_ID).hasRole(ADMIN)
                .antMatchers(HttpMethod.POST, TAGS).hasRole(ADMIN)
                .antMatchers(HttpMethod.PUT, TAGS).hasRole(ADMIN)
                .antMatchers(HttpMethod.DELETE, TAGS_ID).hasRole(ADMIN)
                .antMatchers(HttpMethod.GET, USERS).hasRole(ADMIN)
                .antMatchers(HttpMethod.DELETE, USERS_ID).hasRole(ADMIN)
                .anyRequest().permitAll()

                .and()
                .exceptionHandling()
                .accessDeniedHandler((request, response, e) ->
                {
                    response.setContentType("application/json;charset=UTF-8");
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                    jsonObject.put("message", "Access denied (code 401)");
                    response.getWriter().write(jsonObject.toString());
                })
                .authenticationEntryPoint((request, response, e) ->
                {
                    response.setContentType("application/json;charset=UTF-8");
                    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("timestamp", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
                    jsonObject.put("message", "Access denied (code 403)");
                    response.getWriter().write(jsonObject.toString());
                })

                .and()
                .addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
