package com.epam.esm.exception_handler;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@NoArgsConstructor
@Data
@ToString
public class ErrorMessage implements Serializable {

    private static final long serialVersionUID = -7309643760214061710L;

    private int errorCode;
    private String message;

    public ErrorMessage(int errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }
}
