package com.epam.esm.impl;

import com.epam.esm.additional.CertificateLink;
import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.purchase.PurchaseDto;
import com.epam.esm.impl.purchase.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Map;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value = "/purchases")
@Validated
public class PurchaseController {

    private final PurchaseService purchaseService;

    @Autowired
    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }

    @PostMapping
    public ResponseEntity<PurchaseDto> save(@NotNull @Positive @RequestParam Integer userId,
                                            @NotBlank @RequestParam String certificateIds) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(certificateLink(purchaseService.save(new PurchaseDto(userId, certificateIds))));
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<PurchaseDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        EntityPageDto<PurchaseDto> entityPageDtos = purchaseService.findAll(params);
        entityPageDtos.getEntities().forEach(this::certificateLink);
        return ResponseEntity.ok(entityPageDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PurchaseDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(certificateLink(purchaseService.findById(id)));
    }

    private PurchaseDto certificateLink(PurchaseDto purchase) {
        purchase.getCertificates().forEach(certificate -> purchase
                .add(new CertificateLink(linkTo(CertificateController.class)
                        .slash(certificate.getCertificate().getId()).withRel("certificates"),
                        certificate.getCount())));
        return purchase;
    }
}