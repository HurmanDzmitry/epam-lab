package com.epam.esm.impl.tag;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.dto.tag.TagDtoMapper;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.CreationEntityException;
import com.epam.esm.exception.DeleteEntityException;
import com.epam.esm.exception.EmptyResultException;
import com.epam.esm.exception.UpdateEntityException;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.util.GeneralParamValidation;
import com.epam.esm.util.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.epam.esm.entity.additional.Parameter.SEARCH;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;
    private final TagDtoMapper tagDtoMapper;
    private final GeneralParamValidation generalParamValidation;
    private final GeneralService generalService;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, TagDtoMapper tagDtoMapper, GeneralParamValidation generalParamValidation, GeneralService generalService) {
        this.tagRepository = tagRepository;
        this.tagDtoMapper = tagDtoMapper;
        this.generalParamValidation = generalParamValidation;
        this.generalService = generalService;
    }

    @Transactional
    @Override
    public TagDto update(TagDto dtoEntity) {
        if (!tagRepository.existsById(dtoEntity.getId())) {
            throw new UpdateEntityException("not.created.validation");
        }
        if (tagRepository.existsByName(dtoEntity.getName())) {
            throw new UpdateEntityException("unique.name.validation");
        }
        Tag tag = tagRepository.save(tagDtoMapper.toEntity(dtoEntity));
        return tagDtoMapper.toDto(tag);
    }

    @Transactional
    @Override
    public TagDto save(TagDto dtoEntity) {
        if (tagRepository.existsByName(dtoEntity.getName())) {
            throw new CreationEntityException("unique.name.validation");
        }
        Tag tag = tagRepository.save(tagDtoMapper.toEntity(dtoEntity));
        return tagDtoMapper.toDto(tag);
    }

    @Override
    public EntityPageDto<TagDto> findAll(Map<String, String> params) {
        params = generalParamValidation.validateParams(params, ParameterValueList.TAG_SORTING_FIELDS.getValues());
        Pageable pageable = generalService.getDefaultPageable(params);

        Page<Tag> findTags;
        if (params.containsKey(SEARCH.name())) {
            String search = params.get(SEARCH.name());
            findTags = tagRepository.findDistinctByNameContaining(search, pageable);
        } else {
            findTags = tagRepository.findAll(pageable);
        }

        if (findTags.isEmpty()) {
            throw new EmptyResultException();
        }
        List<TagDto> tags = findTags.get()
                .map(tagDtoMapper::toDto)
                .collect(Collectors.toList());
        return new EntityPageDto<>(tags, (int) findTags.getTotalElements());
    }

    @Override
    public TagDto findById(Integer id) {
        Optional<Tag> tag = tagRepository.findById(id);
        if (!tag.isPresent()) {
            throw new EmptyResultException();
        }
        return tagDtoMapper.toDto(tag.get());
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (!tagRepository.existsById(id)) {
            throw new DeleteEntityException("nothing.delete.validation");
        }
        tagRepository.deleteById(id);
    }

    @Override
    public TagDto findMostExpensiveUserTag(Integer id) {
        Optional<Tag> tag = tagRepository.findMostExpensiveUserTag(id);
        if (!tag.isPresent()) {
            throw new EmptyResultException();
        }
        return tagDtoMapper.toDto(tag.get());
    }
}
