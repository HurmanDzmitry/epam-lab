package com.epam.esm.impl.user;

import com.epam.esm.dto.user.UserDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserDetailsImpl implements UserDetails {

    private static final long serialVersionUID = -5828140369483527774L;

    public static final String ROLE_PREFIX = "ROLE_";

    private String login;
    private String password;
    private Collection<? extends GrantedAuthority> grantedAuthorities;

    public static UserDetailsImpl fromUserDtoToCustomUserDetails(UserDto userDto) {
        UserDetailsImpl c = new UserDetailsImpl();
        c.login = userDto.getLogin();
        c.password = userDto.getPassword();
        c.grantedAuthorities = Collections.singletonList(
                new SimpleGrantedAuthority(ROLE_PREFIX + userDto.getRole().name()));
        return c;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
