package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class CreationEntityException extends RuntimeException {

    private static final long serialVersionUID = -5797198753454326041L;

    public CreationEntityException() {
    }

    public CreationEntityException(String message) {
        super(message);
    }

    public CreationEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public CreationEntityException(Throwable cause) {
        super(cause);
    }
}

