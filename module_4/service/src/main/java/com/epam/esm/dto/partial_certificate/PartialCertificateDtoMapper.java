package com.epam.esm.dto.partial_certificate;

import com.epam.esm.dto.certificate.CertificateDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PartialCertificateDtoMapper {

    private final ModelMapper mapper;

    @Autowired
    public PartialCertificateDtoMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public PartialCertificateDto toPartialDto(CertificateDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, PartialCertificateDto.class);
    }

    public CertificateDto toDto(PartialCertificateDto entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, CertificateDto.class);
    }
}
