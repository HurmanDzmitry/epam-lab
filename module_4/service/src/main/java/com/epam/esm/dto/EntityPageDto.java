package com.epam.esm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class EntityPageDto<T> {

    @NotNull(message = "not.null")
    private List<T> entities;

    @NotNull(message = "not.null")
    @Positive(message = "positive")
    private Integer totalCounts;
}
