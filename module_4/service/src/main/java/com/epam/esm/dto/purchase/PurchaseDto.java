package com.epam.esm.dto.purchase;

import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import com.epam.esm.dto.certificate.CertificateCountDto;
import com.epam.esm.dto.util.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class PurchaseDto extends RepresentationModel<PurchaseDto> implements Serializable {

    private static final long serialVersionUID = -5635213115339907113L;

    @Null(message = "should.null", groups = Saving.class)
    @NotNull(message = "not.null", groups = Updating.class)
    @Positive(message = "positive", groups = Updating.class)
    private Integer id;

    @NotNull(message = "not.null")
    @PastOrPresent(message = "past.present")
    private OffsetDateTime date;

    @JsonSerialize(using = MoneySerializer.class)
    @NotNull(message = "not.null")
    @DecimalMin(value = "0", message = "min")
    @Digits(fraction = 2, integer = 100, message = "not.price")
    private BigDecimal cost;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull(message = "not.null")
    @Positive(message = "positive")
    private Integer userId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String certificateIds;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull(message = "not.null")
    @Positive(message = "positive")
    private Set<CertificateCountDto> certificates;

    public PurchaseDto(Integer userId, String certificateIds) {
        this.userId = userId;
        this.certificateIds = certificateIds;
    }
}
