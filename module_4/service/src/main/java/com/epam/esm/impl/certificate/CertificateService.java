package com.epam.esm.impl.certificate;

import com.epam.esm.ApiService;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.dto.partial_certificate.PartialCertificateDto;

public interface CertificateService extends ApiService<CertificateDto, Integer> {

    CertificateDto partialUpdate(PartialCertificateDto partialCertificateDto);
}
