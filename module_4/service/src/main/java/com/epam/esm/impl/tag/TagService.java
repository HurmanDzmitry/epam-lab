package com.epam.esm.impl.tag;

import com.epam.esm.ApiService;
import com.epam.esm.dto.tag.TagDto;

public interface TagService extends ApiService<TagDto, Integer> {

    TagDto findMostExpensiveUserTag(Integer id);
}
