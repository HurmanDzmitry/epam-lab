package com.epam.esm.dto.certificate;

import com.epam.esm.entity.Certificate;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class CertificateDtoMapper {

    private final ModelMapper mapper;

    @Autowired
    public CertificateDtoMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Certificate toEntity(CertificateDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, Certificate.class);
    }

    public CertificateDto toDto(Certificate entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, CertificateDto.class);
    }
}
