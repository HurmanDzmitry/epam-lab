package com.epam.esm.dto.partial_certificate;

import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.dto.util.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.validator.constraints.CompositionType;
import org.hibernate.validator.constraints.ConstraintComposition;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.math.BigDecimal;
import java.util.Set;

@Data
@NoArgsConstructor
@ToString
public class PartialCertificateDto implements Serializable {

    private static final long serialVersionUID = 4364636286961269216L;

    @NotNull(message = "not.null")
    @Positive(message = "positive")
    private Integer id;

    @ValidSizeOrNull
    private String name;

    @JsonSerialize(using = MoneySerializer.class)
    @PriceOrNull
    private BigDecimal price;

    @RangeOrNull
    private Integer duration;

    private Boolean isActive;

    @JsonIgnore
    private Set<TagDto> tags;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<@NotNull(message = "not.null") Integer> tagIds;


    @ConstraintComposition(CompositionType.OR)
    @ValidPrice
    @Null(message = "should.null")
    @Target({ElementType.METHOD, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = {})
    public @interface PriceOrNull {
        String message() default "not valid";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    @ConstraintComposition(CompositionType.AND)
    @DecimalMin(value = "0", message = "min")
    @Digits(fraction = 2, integer = 100, message = "not.price")
    @Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = {})
    public @interface ValidPrice {
        String message() default "not valid";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    @ConstraintComposition(CompositionType.OR)
    @Min(value = 1, message = "min")
    @Max(value = 365, message = "max")
    @Null(message = "should.null")
    @Target({ElementType.METHOD, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = {})
    public @interface RangeOrNull {
        String message() default "not valid";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }

    @ConstraintComposition(CompositionType.OR)
    @Size(min = 3, max = 300, message = "size")
    @Null(message = "should.null")
    @Target({ElementType.METHOD, ElementType.FIELD})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = {})
    public @interface ValidSizeOrNull {
        String message() default "not valid";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
    }
}