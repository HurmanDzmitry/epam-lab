package com.epam.esm.dto.user;

import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import com.epam.esm.dto.purchase.PurchaseDto;
import com.epam.esm.entity.Role;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class UserDto extends RepresentationModel<UserDto> implements Serializable {

    private static final long serialVersionUID = -6673912807420352406L;

    @Null(message = "should.null", groups = Saving.class)
    @NotNull(message = "not.null", groups = Updating.class)
    @Positive(message = "positive", groups = Updating.class)
    private Integer id;

    @NotBlank(message = "not.blank")
    @Size(min = 3, max = 100, message = "size")
    private String name;

    @NotBlank(message = "not.blank")
    @Size(min = 3, max = 100, message = "size")
    private String surname;

    @NotBlank(message = "not.blank")
    @Size(min = 3, max = 100, message = "size")
    private String login;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(message = "not.blank")
    @Pattern(regexp = "^(?=.*[\\d])(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=\\S+$).{8,100}$",
            message = "password.pattern")
    private String password;

    @NotNull(message = "not.null")
    @Past(message = "past")
    private LocalDate birthday;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Role role;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Set<@Valid @NotNull(message = "not.null") PurchaseDto> purchases;
}
