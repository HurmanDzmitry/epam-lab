CREATE TABLE user
(
  id       INT PRIMARY KEY AUTO_INCREMENT,
  name     VARCHAR(100) NOT NULL,
  surname  VARCHAR(100) NOT NULL,
  login    VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  birthday DATE         NOT NULL,
  role     ENUM ('admin', 'client')
);


CREATE TABLE tag
(
  id   INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL UNIQUE
);


CREATE TABLE certificate
(
  id               INT PRIMARY KEY AUTO_INCREMENT,
  code             INT,
  name             VARCHAR(300) NOT NULL,
  price            DOUBLE       NOT NULL,
  create_date      VARCHAR(100) NOT NULL,
  last_update_date VARCHAR(100) NOT NULL,
  duration         INT          NOT NULL,
  is_active        BOOLEAN      NOT NULL
);


CREATE TABLE certificates_has_tags
(
  id_tag         INT NOT NULL,
  id_certificate INT NOT NULL,
  FOREIGN KEY (id_tag) REFERENCES tag (id),
  FOREIGN KEY (id_certificate) REFERENCES certificate (id),
  UNIQUE (id_tag, id_certificate)
);


CREATE TABLE purchase
(
  id      INT PRIMARY KEY AUTO_INCREMENT,
  id_user INT          NOT NULL,
  date    VARCHAR(100) NOT NULL,
  cost    DOUBLE       NOT NULL,
  FOREIGN KEY (id_user) REFERENCES user (id)
);


CREATE TABLE purchase_has_certificates
(
  id_purchase    INT NOT NULL,
  id_certificate INT NOT NULL,
  FOREIGN KEY (id_purchase) REFERENCES purchase (id),
  FOREIGN KEY (id_certificate) REFERENCES certificate (id)
);


INSERT INTO user
VALUES (1, 'Dima', 'Hurman', 'hurman', '$2a$10$ADYLaPvYDv3.FxOU89iOmOl5GyZjvKTE0gZGRIWmGwJlX9AzXNiMG',
        '1990-11-20', 1), /*Qwerty12345*/
       (2, 'Serj', 'Tankian', 'tankian', '$2a$10$aqbeUC9CZ.chZLbHI87x5ePY.wSLxfSWq11.a6LYWPeJ2rZbYLLSK',
        '1980-09-20', 2), /*12345Abcd*/
       (3, 'Daron', 'Malakian', 'malakian', '$2a$10$.C7aDlM4cDGt8mn18JgIg.QMegjqL5/JDraan1yPxsHMQERS3BYOO',
        '1985-07-20', 2), /*Abcd12345*/
       (4, 'Shavo', 'Odadjian', 'odadjian', '$2a$10$3ghegN8ijCnqyOx0gmq8F.QDN90hyVnmtVUrh2Ezd4kH1qHbRy3bS',
        '1980-05-20', 2), /*Shavo777*/
       (5, 'John', 'Dolmayan', 'dolmayan', '$2a$10$jDhCYKwJtjrDR728iTNrG.vUde.hfthX18SrUAD4.sGxYpLDtPO46',
        '1985-03-20', 2); /*Dolmayan999*/


INSERT INTO tag
VALUES (1, 'beauty'),
       (2, 'entertainment'),
       (3, 'training'),
       (4, 'photoshoot'),
       (5, 'sport'),
       (6, 'quests'),
       (7, 'food'),
       (8, 'travels');


INSERT INTO certificate
VALUES (1, 1, 'Online confectionery master class', 100,
        '2020-08-01T23:59:59-07:00', '2020-08-20T23:59:59+01:00', 50, true),
       (2, 2, 'SPA rituals in Hammam', 200,
        '2020-08-02T23:59:59-06:00', '2020-08-21T23:59:59+02:00', 40, true),
       (3, 3, 'Acting courses', 300,
        '2020-08-03T23:59:59-05:00', '2020-08-22T23:59:59+03:00', 30, true),
       (4, 4, 'Swimming with a dolphin', 400,
        '2020-08-04T23:59:59-04:00', '2020-08-23T23:59:59+04:00', 20, true),
       (5, 5, 'Course "Basics of drawing"', 500,
        '2020-08-05T23:59:59-03:00', '2020-08-24T23:59:59+05:00', 60, true),
       (6, 6, 'Aroma care "Hot cherry"', 600,
        '2020-08-06T23:59:59-02:00', '2020-08-25T23:59:59+06:00', 70, true),
       (7, 7, 'Two-level go-kart track', 700,
        '2020-08-07T23:59:59-01:00', '2020-08-26T23:59:59+07:00', 80, true),
       (8, 8, 'Massage session', 800,
        '2020-08-08T23:59:59+00:00', '2020-08-27T23:59:59+08:00', 90, true),
       (9, 9, 'Tea ceremony', 900,
        '2020-08-09T23:59:59+01:00', '2020-08-28T23:59:59+09:00', 100, true);


INSERT INTO certificates_has_tags
VALUES (7, 1),
       (3, 1),
       (1, 2),
       (3, 3),
       (8, 4),
       (2, 4),
       (3, 5),
       (1, 6),
       (2, 7),
       (5, 7),
       (1, 8),
       (7, 9),
       (3, 9),
       (2, 9);


INSERT INTO purchase
VALUES (1, 5, '2020-10-01T23:00:00+03:00', 900),
       (2, 2, '2020-10-10T22:00:00+05:00', 600),
       (3, 3, '2020-10-20T23:00:00-03:00', 100),
       (4, 4, '2020-10-30T23:00:00-05:00', 800);


INSERT INTO purchase_has_certificates
VALUES (1, 4),
       (1, 5),
       (2, 3),
       (2, 3),
       (3, 1),
       (4, 8)