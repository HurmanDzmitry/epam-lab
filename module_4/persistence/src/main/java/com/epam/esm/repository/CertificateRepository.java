package com.epam.esm.repository;

import com.epam.esm.entity.Certificate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Set;

public interface CertificateRepository extends JpaRepository<Certificate, Integer>,
        CrudRepository<Certificate, Integer>,
        PagingAndSortingRepository<Certificate, Integer> {

    Page<Certificate> findDistinctByNameContaining(String name, Pageable pageable);

    @Query("select distinct c from Certificate c join c.tags t where t.name in :names")
    Page<Certificate> findDistinctByTagNames(Set<String> names, Pageable pageable);
}
