package com.epam.esm.entity.custom_hibernate_type;

import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.java.ImmutableMutabilityPlan;

import java.time.OffsetDateTime;

public class OffsetDateTimeStringJavaDescriptor extends AbstractTypeDescriptor<OffsetDateTime> {

    private static final long serialVersionUID = 6185068662166405144L;

    public static final OffsetDateTimeStringJavaDescriptor INSTANCE = new OffsetDateTimeStringJavaDescriptor();

    public OffsetDateTimeStringJavaDescriptor() {
        super(OffsetDateTime.class, ImmutableMutabilityPlan.INSTANCE);
    }

    @Override
    public String toString(OffsetDateTime value) {
        return value.toString();
    }

    @Override
    public OffsetDateTime fromString(String string) {
        return OffsetDateTime.parse(string);
    }

    @Override
    public <X> X unwrap(OffsetDateTime value, Class<X> type, WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (String.class.isAssignableFrom(type)) {
            return (X) value.toString();
        }
        throw unknownUnwrap(type);
    }

    @Override
    public <X> OffsetDateTime wrap(X value, WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return OffsetDateTime.parse((CharSequence) value);
        }
        throw unknownWrap(value.getClass());
    }
}
