package com.epam.esm.repository;

import com.epam.esm.entity.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface TagRepository extends JpaRepository<Tag, Integer>,
        CrudRepository<Tag, Integer>,
        PagingAndSortingRepository<Tag, Integer> {

    Optional<Tag> findByName(String name);

    boolean existsByName(String name);

    Page<Tag> findDistinctByNameContaining(String name, Pageable pageable);

    @Query(value = "select * " +
            "from tag " +
            "       join certificates_has_tags cht on tag.id = cht.id_tag " +
            "       join certificate c on cht.id_certificate = c.id " +
            "       join purchase_has_certificates phc on c.id = phc.id_certificate " +
            "       join purchase p on phc.id_purchase = p.id " +
            "       join user u on p.id_user = u.id " +
            "where u.id = :id " +
            "group by tag.name " +
            "order by sum(p.cost) desc " +
            "limit 1",
            nativeQuery = true)
    Optional<Tag> findMostExpensiveUserTag(Integer id);
}
