package com.epam.esm.repository;

import com.epam.esm.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer>,
        CrudRepository<User, Integer>,
        PagingAndSortingRepository<User, Integer> {

    Optional<User> findByLogin(String login);

    boolean existsByLogin(String login);

    Page<User> findDistinctByNameContainingOrSurnameContainingOrLoginContaining(
            String name, String surname, String login, Pageable pageable);
}
