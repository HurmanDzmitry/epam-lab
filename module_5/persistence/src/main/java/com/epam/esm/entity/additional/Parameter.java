package com.epam.esm.entity.additional;

public enum Parameter {

    SEPARATOR(","),

    SORT_BY("id"),
    SORT_HOW("asc"),
    PAGE("0"),
    PAGE_SIZE("10"),

    SEARCH(""),
    USER_ID(""),
    TAG_NAMES("");

    private String defaultValue;

    Parameter(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
