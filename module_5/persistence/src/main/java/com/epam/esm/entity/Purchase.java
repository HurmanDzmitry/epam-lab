package com.epam.esm.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = "user")
@ToString(exclude = "user")
public class Purchase implements Serializable {

    private static final long serialVersionUID = 2271077215375636008L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(updatable = false)
    @Type(type = "com.epam.esm.entity.custom_hibernate_type.OffsetDateTimeStringType")
    private OffsetDateTime date;

    @Column(updatable = false)
    private BigDecimal cost;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "purchase_has_certificates",
            joinColumns = {@JoinColumn(name = "id_purchase")},
            inverseJoinColumns = {@JoinColumn(name = "id_certificate")})
    private List<Certificate> certificates;

    public Purchase(OffsetDateTime date, BigDecimal cost, User user, List<Certificate> certificates) {
        this.date = date;
        this.cost = cost;
        this.user = user;
        this.certificates = certificates;
    }
}
