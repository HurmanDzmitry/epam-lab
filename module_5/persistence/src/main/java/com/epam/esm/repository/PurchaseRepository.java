package com.epam.esm.repository;

import com.epam.esm.entity.Purchase;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer>,
        CrudRepository<Purchase, Integer>,
        PagingAndSortingRepository<Purchase, Integer> {

    Page<Purchase> findByUserId(Integer id, Pageable pageable);
}
