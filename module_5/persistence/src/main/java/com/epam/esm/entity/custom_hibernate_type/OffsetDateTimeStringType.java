package com.epam.esm.entity.custom_hibernate_type;

import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

import java.time.OffsetDateTime;

public class OffsetDateTimeStringType
        extends AbstractSingleColumnStandardBasicType<OffsetDateTime> {

    private static final long serialVersionUID = 8707772382453501087L;

    public static final OffsetDateTimeStringType INSTANCE = new OffsetDateTimeStringType();

    public OffsetDateTimeStringType() {
        super(VarcharTypeDescriptor.INSTANCE, OffsetDateTimeStringJavaDescriptor.INSTANCE);
    }

    @Override
    public String getName() {
        return "OffsetDateTimeString";
    }
}
