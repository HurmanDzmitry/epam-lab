package com.epam.esm.exception_handler;

import com.epam.esm.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Objects;

import static com.epam.esm.exception_handler.ErrorResponse.*;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @Autowired
    public ExceptionHandlerController(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(CreationEntityException.class)
    public ResponseEntity<ErrorMessage> handleCreationEntityException(CreationEntityException ex) {
        String message = (ex.getMessage() == null || ex.getMessage().isEmpty())
                ? CREATION_ENTITY_RESPONSE.getMessage()
                : ex.getMessage();
        return new ResponseEntity<>(new ErrorMessage(CREATION_ENTITY_RESPONSE.getCode(),
                messageSource.getMessage(message, null, LocaleContextHolder.getLocale())),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UpdateEntityException.class)
    public ResponseEntity<ErrorMessage> handleUpdateEntityException(UpdateEntityException ex) {
        return new ResponseEntity<>(new ErrorMessage(UPDATE_ENTITY_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? UPDATE_ENTITY_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DeleteEntityException.class)
    public ResponseEntity<ErrorMessage> handleDeleteEntityException(DeleteEntityException ex) {
        String message = (ex.getMessage() == null || ex.getMessage().isEmpty())
                ? DELETE_ENTITY_RESPONSE.getMessage()
                : ex.getMessage();
        return new ResponseEntity<>(new ErrorMessage(DELETE_ENTITY_RESPONSE.getCode(),
                messageSource.getMessage(message, null, LocaleContextHolder.getLocale())),
                HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        StringBuilder sb = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = (error.getDefaultMessage() == null || error.getDefaultMessage().isEmpty())
                    ? messageSource.getMessage(VALIDATION_PARAM_RESPONSE.getMessage(), null,
                    LocaleContextHolder.getLocale())
                    : messageSource.getMessage(error.getDefaultMessage(), error.getArguments(),
                    LocaleContextHolder.getLocale());
            sb.append(fieldName).append(": ").append(errorMessage).append("; ");
        });
        sb.deleteCharAt(sb.length() - 1);
        String message = sb.toString();
        return new ResponseEntity<>(new ErrorMessage(VALIDATION_ENTITY_RESPONSE.getCode(),
                (message.isEmpty())
                        ? messageSource.getMessage(VALIDATION_ENTITY_RESPONSE.getMessage(), null,
                        LocaleContextHolder.getLocale())
                        : message),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ConstraintViolationException.class, ValidationParamException.class})
    public ResponseEntity<ErrorMessage> handleConstraintViolationException(RuntimeException ex) {
        String message = (ex.getMessage() == null || ex.getMessage().isEmpty())
                ? messageSource.getMessage(VALIDATION_PARAM_RESPONSE.getMessage(), null,
                LocaleContextHolder.getLocale())
                : ex.getMessage();
        return new ResponseEntity<>(new ErrorMessage(VALIDATION_PARAM_RESPONSE.getCode(), message),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyResultException.class)
    public ResponseEntity<ErrorMessage> handleEmptyResultException(EmptyResultException ex) {
        String message = (ex.getMessage() == null || ex.getMessage().isEmpty())
                ? EMPTY_RESULT_RESPONSE.getMessage()
                : ex.getMessage();
        return new ResponseEntity<>(new ErrorMessage(EMPTY_RESULT_RESPONSE.getCode(),
                messageSource.getMessage(message, null, LocaleContextHolder.getLocale())),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InvalidCredentialsException.class)
    public ResponseEntity<ErrorMessage> handleInvalidCredentialsException(InvalidCredentialsException ex) {
        String message = (ex.getMessage() == null || ex.getMessage().isEmpty())
                ? INVALID_CREDENTIALS_RESPONSE.getMessage()
                : ex.getMessage();
        return new ResponseEntity<>(new ErrorMessage(INVALID_CREDENTIALS_RESPONSE.getCode(),
                messageSource.getMessage(message, null, LocaleContextHolder.getLocale())),
                HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ErrorMessage> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        return new ResponseEntity<>(new ErrorMessage(VALIDATION_PARAM_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? messageSource.getMessage(VALIDATION_PARAM_RESPONSE.getMessage(), null,
                        LocaleContextHolder.getLocale())
                        : ex.getName() + (": ") + (Objects.requireNonNull(ex.getValue()).toString()) + ";"),
                HttpStatus.BAD_REQUEST);
    }
}