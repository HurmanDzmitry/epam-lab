package com.epam.esm.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.Date;

@Component
public class JwtProvider {

    @Value("${spring.security.secret.jwt}")
    private String secretJWT;
    @Value("${spring.security.expiration.time.min}")
    private int expirationTime;

    public String generateToken(String login) {
        Date date = Date.from(OffsetDateTime.now().plusMinutes(expirationTime).toInstant());
        return Jwts.builder()
                .setSubject(login)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS512, secretJWT)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(secretJWT).parseClaimsJws(token);
            return true;
        } catch (Exception exc) {
            return false;
        }
    }

    public String getLoginFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(secretJWT).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }
}
