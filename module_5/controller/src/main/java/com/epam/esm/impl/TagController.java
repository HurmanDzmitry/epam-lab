package com.epam.esm.impl;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.impl.tag.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.groups.Default;
import java.util.Map;

@RestController
@RequestMapping("/tags")
@Validated
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping
    public ResponseEntity<TagDto> save(@Validated({Saving.class, Default.class})
                                       @NotNull @RequestBody TagDto request) {
        return ResponseEntity.status(HttpStatus.CREATED).body(tagService.save(request));
    }

    @PutMapping
    public ResponseEntity<TagDto> update(@Validated({Updating.class, Default.class})
                                         @NotNull @RequestBody TagDto request) {
        return ResponseEntity.ok(tagService.update(request));
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<TagDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        return ResponseEntity.ok(tagService.findAll(params));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TagDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(tagService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteById(@NotNull @Positive @PathVariable Integer id) {
        tagService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/purchases/users/{id}")
    public ResponseEntity<TagDto> findMostExpensiveUserTag(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(tagService.findMostExpensiveUserTag(id));
    }
}
