package com.epam.esm.additional;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.hateoas.Link;

@Data
@EqualsAndHashCode(callSuper = true)
public class CertificateLink extends Link {

    private static final long serialVersionUID = 2759564081103486528L;

    private Integer count;

    public CertificateLink(Link link, Integer count) {
        super(link.getHref(), link.getRel());
        this.count = count;
    }
}
