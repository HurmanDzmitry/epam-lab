package com.epam.esm.config.security;

import com.epam.esm.entity.Role;

public class Constants {

    /**
     * Roles
     */
    public static final String ADMIN = Role.ADMIN.name();
    public static final String CLIENT = Role.CLIENT.name();

    /**
     * Endpoints
     */
    public static final String CERTIFICATES = "/certificates";
    public static final String CERTIFICATES_ID = "/certificates/{id}";

    public static final String PURCHASES = "/purchases";
    public static final String PURCHASES_ID = "/purchases/{id}";

    public static final String TAGS = "/tags";
    public static final String TAGS_ID = "/tags/{id}";
    public static final String TAGS_PURCHASES_USERS_ID = "/tags/purchases/users/{id}";

    public static final String USERS = "/users";
    public static final String USERS_ID = "/users/{id}";
    public static final String USERS_SIGNIN = "/users/signIn";
}
