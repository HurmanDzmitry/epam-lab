package com.epam.esm.impl;

import com.epam.esm.config.security.JwtProvider;
import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import com.epam.esm.dto.auth.AuthRequestDto;
import com.epam.esm.dto.auth.AuthResponseDto;
import com.epam.esm.dto.user.UserDto;
import com.epam.esm.impl.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.groups.Default;
import java.util.Map;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
@RequestMapping(value = "/users")
@Validated
public class UserController {

    private final UserService userService;
    private final JwtProvider jwtProvider;

    @Autowired
    public UserController(UserService userService, JwtProvider jwtProvider) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping
    public ResponseEntity<AuthResponseDto> save(@Validated({Saving.class, Default.class})
                                                @NotNull @RequestBody UserDto user) {
        UserDto userDto = userService.save(user);
        String token = jwtProvider.generateToken(userDto.getLogin());
        return ResponseEntity.status(HttpStatus.CREATED).body(new AuthResponseDto(token));
    }

    @PutMapping
    public ResponseEntity<UserDto> update(@Validated({Updating.class, Default.class})
                                          @NotNull @RequestBody UserDto user) {
        return ResponseEntity.ok(purchaseLink(userService.update(user)));
    }

    @PostMapping("/signIn")
    public ResponseEntity<AuthResponseDto> signIn(@Valid @NotNull @RequestBody AuthRequestDto authRequestDto) {
        UserDto userDto = userService.findByLoginAndPassword(authRequestDto);
        String token = jwtProvider.generateToken(userDto.getLogin());
        return ResponseEntity.ok(new AuthResponseDto(token));
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<UserDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        EntityPageDto<UserDto> entityPageDtos = userService.findAll(params);
        entityPageDtos.getEntities().forEach(this::purchaseLink);
        return ResponseEntity.ok(entityPageDtos);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(purchaseLink(userService.findById(id)));
    }

    private UserDto purchaseLink(UserDto user) {
        user.getPurchases().forEach(purchase -> user.add(linkTo(PurchaseController.class)
                .slash(purchase.getId()).withRel("purchases")));
        return user;
    }
}