package com.epam.esm.dto.tag;

import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@ToString
public class TagDto implements Serializable {

    private static final long serialVersionUID = -3289018326007960344L;

    @Null(message = "should.null", groups = Saving.class)
    @NotNull(message = "not.null", groups = Updating.class)
    @Positive(message = "positive", groups = Updating.class)
    private Integer id;

    @NotNull(message = "not.null")
    @Size(min = 3, max = 100, message = "size")
    private String name;
}