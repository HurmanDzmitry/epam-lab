package com.epam.esm.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@AllArgsConstructor
public class AuthRequestDto implements Serializable {

    private static final long serialVersionUID = 254156031647103381L;

    @NotBlank(message = "not.blank")
    private String login;

    @NotBlank(message = "not.blank")
    private String password;
}
