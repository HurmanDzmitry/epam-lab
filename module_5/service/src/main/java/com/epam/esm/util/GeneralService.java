package com.epam.esm.util;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.epam.esm.entity.additional.Parameter.*;

@Component
public class GeneralService {

    public Pageable getDefaultPageable(Map<String, String> params) {
        return PageRequest.of(Integer.parseInt(params.get(PAGE.name())),
                Integer.parseInt(params.get(PAGE_SIZE.name())),
                Sort.by(Sort.Direction.fromString(params.get(SORT_HOW.name())),
                        params.get(SORT_BY.name())));
    }
}
