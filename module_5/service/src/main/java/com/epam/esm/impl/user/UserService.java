package com.epam.esm.impl.user;

import com.epam.esm.ApiService;
import com.epam.esm.dto.auth.AuthRequestDto;
import com.epam.esm.dto.user.UserDto;

public interface UserService extends ApiService<UserDto, Integer> {

    UserDto findByLogin(String login);

    UserDto findByLoginAndPassword(AuthRequestDto authRequestDto);
}
