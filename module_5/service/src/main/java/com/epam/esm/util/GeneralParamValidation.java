package com.epam.esm.util;

import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.ValidationParamException;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.epam.esm.entity.additional.Parameter.*;

@Component
public class GeneralParamValidation {

    private Map<String, String> inputParams;

    private final MessageSource messageSource;

    public GeneralParamValidation(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public Map<String, String> validateParams(Map<String, String> params, List<String> validSortingValues) {
        inputParams = params.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        i -> i.getKey().toUpperCase(),
                        i -> i.getValue().toLowerCase()));
        sortByValueValidate(validSortingValues);
        sortHowValueValidate();
        paginationValidate();

        return inputParams;
    }

    private void sortByValueValidate(List<String> validSortingValues) {
        String value = primaryValidate(SORT_BY.name(), SORT_BY.getDefaultValue());
        if (!validSortingValues.contains(value)) {
            throw new ValidationParamException(SORT_BY.name() + " = \"" + value + "\" "
                    + messageSource.getMessage("not.match.valid.values", null, LocaleContextHolder.getLocale()));
        }
    }

    private void sortHowValueValidate() {
        String value = primaryValidate(SORT_HOW.name(), SORT_HOW.getDefaultValue());
        if (!ParameterValueList.SORTING_DIRECTION.getValues().contains(value)) {
            throw new ValidationParamException(SORT_HOW.name() + " = \"" + value + "\" "
                    + messageSource.getMessage("not.match.valid.values", null, LocaleContextHolder.getLocale()));
        }
    }

    private void paginationValidate() {
        String value = primaryValidate(PAGE_SIZE.name(), PAGE_SIZE.getDefaultValue());
        int pageSize;
        try {
            pageSize = Integer.parseInt(value);
            if (pageSize < 1) {
                throw new ValidationParamException(PAGE_SIZE.name() + " = \"" + value + "\" "
                        + messageSource.getMessage("less.than", null, LocaleContextHolder.getLocale())
                        + " 1.");
            }
        } catch (NumberFormatException e) {
            throw new ValidationParamException(PAGE_SIZE.name() + " = \"" + value + "\" "
                    + messageSource.getMessage("not.integer", null, LocaleContextHolder.getLocale()));
        }

        value = primaryValidate(PAGE.name(), PAGE.getDefaultValue());
        try {
            int page = Integer.parseInt(value);
            if (page < Integer.parseInt(PAGE.getDefaultValue())) {
                throw new ValidationParamException(PAGE.name() + " = \"" + value + "\" "
                        + messageSource.getMessage("not.positive.integer", null, LocaleContextHolder.getLocale()));
            }
        } catch (NumberFormatException e) {
            throw new ValidationParamException(PAGE.name() + " = \"" + value + "\" "
                    + messageSource.getMessage("not.integer", null, LocaleContextHolder.getLocale()));
        }
    }

    private String primaryValidate(String param, String defaultValue) {
        String value;
        if (inputParams.containsKey(param)) {
            value = inputParams.get(param);
            if (value == null || value.trim().isEmpty()) {
                throw new ValidationParamException(param + " = "
                        + messageSource.getMessage((value == null ? "null" : "empty"),
                        null, LocaleContextHolder.getLocale()));
            }
        } else {
            value = defaultValue;
        }
        inputParams.put(param, value);
        return inputParams.get(param);
    }
}
