package com.epam.esm.dto.certificate;

import com.epam.esm.dto.Saving;
import com.epam.esm.dto.Updating;
import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.dto.util.MoneySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.hateoas.RepresentationModel;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class CertificateDto extends RepresentationModel<CertificateDto> implements Serializable {

    private static final long serialVersionUID = 5489046900254811286L;

    @Null(message = "should.null", groups = Saving.class)
    @NotNull(message = "not.null", groups = Updating.class)
    @Positive(message = "positive", groups = Updating.class)
    private Integer id;

    @NotNull(message = "not_null.")
    @Size(min = 3, max = 300, message = "size")
    private String name;

    @JsonSerialize(using = MoneySerializer.class)
    @NotNull(message = "not.null")
    @DecimalMin(value = "0", message = "min")
    @Digits(fraction = 2, integer = 100, message = "not.price")
    private BigDecimal price;

    @NotNull(message = "not.null")
    @Min(value = 1, message = "min")
    @Max(value = 365, message = "max")
    private Integer duration;

    @NotNull(message = "not.null")
    private Boolean isActive;

    @JsonIgnore
    private Set<TagDto> tags;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull(message = "not.null")
    @NotEmpty(message = "not.empty")
    private Set<@NotNull(message = "not.null") Integer> tagIds;
}