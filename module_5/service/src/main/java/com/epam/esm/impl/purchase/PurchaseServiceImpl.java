package com.epam.esm.impl.purchase;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.purchase.PurchaseDto;
import com.epam.esm.dto.purchase.PurchaseDtoMapper;
import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Purchase;
import com.epam.esm.entity.Role;
import com.epam.esm.entity.User;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.*;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.PurchaseRepository;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.util.GeneralParamValidation;
import com.epam.esm.util.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.epam.esm.entity.additional.Parameter.USER_ID;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    private final PurchaseRepository purchaseRepository;
    private final UserRepository userRepository;
    private final CertificateRepository certificateRepository;
    private final PurchaseDtoMapper purchaseDtoMapper;
    private final GeneralParamValidation generalParamValidation;
    private final GeneralService generalService;

    @Autowired
    public PurchaseServiceImpl(PurchaseRepository purchaseRepository, UserRepository userRepository,
                               CertificateRepository certificateRepository, PurchaseDtoMapper purchaseDtoMapper, GeneralParamValidation generalParamValidation, GeneralService generalService) {
        this.purchaseRepository = purchaseRepository;
        this.userRepository = userRepository;
        this.certificateRepository = certificateRepository;
        this.purchaseDtoMapper = purchaseDtoMapper;
        this.generalParamValidation = generalParamValidation;
        this.generalService = generalService;
    }

    @Transactional
    @Override
    public PurchaseDto update(PurchaseDto dtoEntity) {
        Optional<Purchase> purchase = purchaseRepository.findById(dtoEntity.getId());
        if (!purchase.isPresent()) {
            throw new UpdateEntityException("unique.id.validation");
        }
        Purchase updatePurchase = purchaseRepository.save(purchase.get());
        return purchaseDtoMapper.toDto(updatePurchase);
    }

    @Transactional
    @Override
    public PurchaseDto save(PurchaseDto dtoEntity) {
        String[] split = dtoEntity.getCertificateIds().split(",");
        List<Integer> certificateIds;
        try {
            certificateIds = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        } catch (NumberFormatException e) {
            throw new CreationEntityException("not.integer.validation");
        }

        Optional<User> user = userRepository.findById(dtoEntity.getUserId());
        if (!user.isPresent()) {
            throw new CreationEntityException("not.created.validation");
        }
        if (user.get().getRole() == Role.ADMIN) {
            throw new CreationEntityException("user.admin");
        }

        List<Certificate> certificates = certificateIds.stream().map(id -> {
            Optional<Certificate> optionalCertificate = certificateRepository.findById(id);
            if (!optionalCertificate.isPresent()) {
                throw new CreationEntityException("not.created.validation");
            }
            Certificate certificate = optionalCertificate.get();
            if (!certificate.isActive()) {
                throw new CreationEntityException("non.active.certificate");
            }
            return optionalCertificate.get();
        }).collect(Collectors.toList());

        BigDecimal cost = certificates.stream().map(Certificate::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        Purchase purchase = new Purchase(OffsetDateTime.now(), cost, user.get(), certificates);
        purchase = purchaseRepository.save(purchase);
        return purchaseDtoMapper.toDto(purchase);
    }

    @Override
    public EntityPageDto<PurchaseDto> findAll(Map<String, String> params) {
        params = generalParamValidation.validateParams(params, ParameterValueList.PURCHASE_SORTING_FIELDS.getValues());
        Pageable pageable = generalService.getDefaultPageable(params);

        Page<Purchase> findPurchases;
        if (params.containsKey(USER_ID.name())) {
            int userId;
            try {
                userId = Integer.parseInt(params.get(USER_ID.name()));
                if (userId <= 0) {
                    throw new ValidationParamException("id.validation");
                }
            } catch (NumberFormatException e) {
                throw new ValidationParamException("not.integer.validation");
            }
            findPurchases = purchaseRepository.findByUserId(userId, pageable);
        } else {
            findPurchases = purchaseRepository.findAll(pageable);
        }
        if (findPurchases.isEmpty()) {
            throw new EmptyResultException();
        }
        List<PurchaseDto> purchases = findPurchases.get()
                .map(purchaseDtoMapper::toDto)
                .collect(Collectors.toList());
        return new EntityPageDto<>(purchases, (int) findPurchases.getTotalElements());
    }

    @Override
    public PurchaseDto findById(Integer id) {
        Optional<Purchase> purchase = purchaseRepository.findById(id);
        if (!purchase.isPresent()) {
            throw new EmptyResultException();
        }
        return purchaseDtoMapper.toDto(purchase.get());
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (!purchaseRepository.existsById(id)) {
            throw new DeleteEntityException("nothing.delete.validation");
        }
        purchaseRepository.deleteById(id);
    }
}
