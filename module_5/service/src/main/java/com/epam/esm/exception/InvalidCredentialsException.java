package com.epam.esm.exception;

public class InvalidCredentialsException extends RuntimeException {

    private static final long serialVersionUID = 7318408758663201956L;

    public InvalidCredentialsException() {
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }

    public InvalidCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCredentialsException(Throwable cause) {
        super(cause);
    }
}
