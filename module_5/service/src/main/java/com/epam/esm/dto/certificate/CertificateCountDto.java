package com.epam.esm.dto.certificate;

import com.epam.esm.entity.Certificate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CertificateCountDto implements Serializable {

    private static final long serialVersionUID = 7732662734869206352L;

    @Valid
    @NotNull(message = "not.null")
    private Certificate certificate;

    @NotNull(message = "not.null")
    @Positive(message = "positive")
    private Integer count;
}
