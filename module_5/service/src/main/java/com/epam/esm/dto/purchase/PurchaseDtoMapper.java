package com.epam.esm.dto.purchase;

import com.epam.esm.dto.certificate.CertificateCountDto;
import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Purchase;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class PurchaseDtoMapper {

    private final ModelMapper mapper;

    @Autowired
    public PurchaseDtoMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Purchase toEntity(PurchaseDto dto) {
        Purchase purchase = Objects.isNull(dto) ? null : mapper.map(dto, Purchase.class);
        Set<CertificateCountDto> certificateCountDtoSet = dto.getCertificates();
        List<Certificate> certificates = new ArrayList<>();
        certificateCountDtoSet.forEach(i -> IntStream.range(0, i.getCount())
                .forEach(j -> certificates.add(i.getCertificate())));
        purchase.setCertificates(certificates);
        return purchase;
    }

    public PurchaseDto toDto(Purchase entity) {
        PurchaseDto purchaseDto = Objects.isNull(entity) ? null : mapper.map(entity, PurchaseDto.class);
        List<Certificate> certificates = entity.getCertificates();
        Set<CertificateCountDto> certificateCountDtos = certificates.stream().map(i -> {
            int count = Collections.frequency(certificates, i);
            return new CertificateCountDto(i, count);
        }).collect(Collectors.toSet());
        purchaseDto.setCertificates(certificateCountDtos);
        return purchaseDto;
    }
}