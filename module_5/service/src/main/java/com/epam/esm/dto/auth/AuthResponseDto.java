package com.epam.esm.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class AuthResponseDto implements Serializable {

    private static final long serialVersionUID = -5824271469141660251L;

    private String token;
}
