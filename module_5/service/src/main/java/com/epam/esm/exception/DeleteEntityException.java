package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class DeleteEntityException extends RuntimeException {

    private static final long serialVersionUID = 5902796660301138534L;

    public DeleteEntityException() {
    }

    public DeleteEntityException(String message) {
        super(message);
    }

    public DeleteEntityException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeleteEntityException(Throwable cause) {
        super(cause);
    }
}

