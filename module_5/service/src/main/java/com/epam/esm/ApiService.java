package com.epam.esm;

import com.epam.esm.dto.EntityPageDto;

import java.util.Map;

public interface ApiService<T, K> {

    T update(T dtoEntity);

    T save(T dtoEntity);

    EntityPageDto<T> findAll(Map<String, String> params);

    T findById(K id);

    void deleteById(K id);
}
