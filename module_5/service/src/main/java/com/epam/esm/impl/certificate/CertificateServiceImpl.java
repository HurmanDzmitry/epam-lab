package com.epam.esm.impl.certificate;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.dto.certificate.CertificateDtoMapper;
import com.epam.esm.dto.partial_certificate.PartialCertificateDto;
import com.epam.esm.dto.partial_certificate.PartialCertificateDtoMapper;
import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.DeleteEntityException;
import com.epam.esm.exception.EmptyResultException;
import com.epam.esm.exception.UpdateEntityException;
import com.epam.esm.exception.ValidationParamException;
import com.epam.esm.repository.CertificateRepository;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.util.GeneralParamValidation;
import com.epam.esm.util.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.epam.esm.entity.additional.Parameter.*;

@Service
public class CertificateServiceImpl implements CertificateService {

    private final CertificateRepository certificateRepository;
    private final CertificateDtoMapper certificateDtoMapper;
    private final PartialCertificateDtoMapper partialCertificateDtoMapper;
    private final TagRepository tagRepository;
    private final GeneralParamValidation generalParamValidation;
    private final GeneralService generalService;

    @Autowired
    public CertificateServiceImpl(CertificateRepository certificateRepository, CertificateDtoMapper certificateDtoMapper,
                                  PartialCertificateDtoMapper partialCertificateDtoMapper, TagRepository tagRepository,
                                  GeneralParamValidation generalParamValidation, GeneralService generalService) {
        this.certificateRepository = certificateRepository;
        this.certificateDtoMapper = certificateDtoMapper;
        this.partialCertificateDtoMapper = partialCertificateDtoMapper;
        this.tagRepository = tagRepository;
        this.generalParamValidation = generalParamValidation;
        this.generalService = generalService;
    }

    @Transactional
    @Override
    public CertificateDto update(CertificateDto dtoEntity) {
        Optional<Certificate> optionalCertificate = certificateRepository.findById(dtoEntity.getId());
        if (!optionalCertificate.isPresent()) {
            throw new UpdateEntityException("not.created.validation");
        }
        Certificate certificate = optionalCertificate.get();
        OffsetDateTime createDate = certificate.getCreateDate();
        OffsetDateTime lastUpdateDate = OffsetDateTime.now();
        int code = certificate.getCode();
        certificate.setLastUpdateDate(lastUpdateDate);
        certificate.setActive(false);
        certificateRepository.save(certificate);

        certificate = certificateDtoMapper.toEntity(dtoEntity);
        certificate.setTags(findAndValidateTagByIds(dtoEntity.getTagIds()));
        certificate.setCreateDate(createDate);
        certificate.setLastUpdateDate(lastUpdateDate);
        certificate.setCode(code);
        certificate.setActive(true);
        certificate.setId(null);
        certificateRepository.save(certificate);
        return certificateDtoMapper.toDto(certificate);
    }

    @Transactional
    @Override
    public CertificateDto save(CertificateDto dtoEntity) {
        Certificate certificate = certificateDtoMapper.toEntity(dtoEntity);
        certificate.setTags(findAndValidateTagByIds(dtoEntity.getTagIds()));
        certificate.setCreateDate(OffsetDateTime.now());
        certificate.setLastUpdateDate(certificate.getCreateDate());
        certificate.setActive(true);
        certificate = certificateRepository.save(certificate);

        int code = certificate.getId();
        certificate.setCode(code);
        certificateRepository.save(certificate);
        return certificateDtoMapper.toDto(certificate);
    }

    @Override
    public EntityPageDto<CertificateDto> findAll(Map<String, String> params) {
        params = generalParamValidation.validateParams(params, ParameterValueList.CERTIFICATE_SORTING_FIELDS.getValues());
        Pageable pageable = generalService.getDefaultPageable(params);

        Page<Certificate> findCertificates;
        if (params.containsKey(TAG_NAMES.name())) {
            String tagNames = params.get(TAG_NAMES.name());
            findCertificates = certificateRepository.findDistinctByTagNames(Set.of(tagNames.split(SEPARATOR.getDefaultValue())),
                    pageable);
        } else if (params.containsKey(SEARCH.name())) {
            String search = params.get(SEARCH.name());
            findCertificates = certificateRepository.findDistinctByNameContaining(search, pageable);
        } else {
            findCertificates = certificateRepository.findAll(pageable);
        }

        if (findCertificates.isEmpty()) {
            throw new EmptyResultException();
        }
        List<CertificateDto> certificates = findCertificates.get()
                .map(certificateDtoMapper::toDto)
                .collect(Collectors.toList());
        return new EntityPageDto<>(certificates, (int) findCertificates.getTotalElements());
    }

    @Override
    public CertificateDto findById(Integer id) {
        Optional<Certificate> certificate = certificateRepository.findById(id);
        if (!certificate.isPresent()) {
            throw new EmptyResultException();
        }
        return certificateDtoMapper.toDto(certificate.get());
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (!certificateRepository.existsById(id)) {
            throw new DeleteEntityException("nothing.delete.validation");
        }
        certificateRepository.deleteById(id);
    }

    @Override
    public CertificateDto partialUpdate(PartialCertificateDto partialCertificateDto) {
        Optional<Certificate> optionalCertificate = certificateRepository.findById(partialCertificateDto.getId());
        if (!optionalCertificate.isPresent()) {
            throw new UpdateEntityException("not.created.validation");
        }
        Certificate certificate = optionalCertificate.get();
        if (partialCertificateDto.getName() == null || partialCertificateDto.getName().isEmpty()) {
            partialCertificateDto.setName(certificate.getName());
        }
        if (partialCertificateDto.getPrice() == null) {
            partialCertificateDto.setPrice(certificate.getPrice());
        }
        if (partialCertificateDto.getDuration() == null) {
            partialCertificateDto.setDuration(certificate.getDuration());
        }
        if (partialCertificateDto.getIsActive() == null) {
            partialCertificateDto.setIsActive(certificate.isActive());
        }
        if (partialCertificateDto.getTagIds() == null || partialCertificateDto.getTagIds().isEmpty()) {
            partialCertificateDto.setTagIds(certificate.getTags().stream().map(Tag::getId).collect(Collectors.toSet()));
        }
        return update(partialCertificateDtoMapper.toDto(partialCertificateDto));
    }

    private Set<Tag> findAndValidateTagByIds(Set<Integer> tagIds) {
        return tagIds.stream().map(id -> {
            Optional<Tag> tag = tagRepository.findById(id);
            if (!tag.isPresent()) {
                throw new ValidationParamException();
            }
            return tag.get();
        }).collect(Collectors.toSet());
    }
}