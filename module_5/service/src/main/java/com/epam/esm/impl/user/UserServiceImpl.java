package com.epam.esm.impl.user;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.auth.AuthRequestDto;
import com.epam.esm.dto.user.UserDto;
import com.epam.esm.dto.user.UserDtoMapper;
import com.epam.esm.entity.Role;
import com.epam.esm.entity.User;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.*;
import com.epam.esm.repository.UserRepository;
import com.epam.esm.util.GeneralParamValidation;
import com.epam.esm.util.GeneralService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.epam.esm.entity.additional.Parameter.SEARCH;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final UserDtoMapper userDtoMapper;
    private final PasswordEncoder passwordEncoder;
    private final GeneralParamValidation generalParamValidation;
    private final GeneralService generalService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserDtoMapper userDtoMapper, PasswordEncoder passwordEncoder, GeneralParamValidation generalParamValidation, GeneralService generalService) {
        this.userRepository = userRepository;
        this.userDtoMapper = userDtoMapper;
        this.passwordEncoder = passwordEncoder;
        this.generalParamValidation = generalParamValidation;
        this.generalService = generalService;
    }

    @Transactional
    @Override
    public UserDto update(UserDto dtoEntity) {
        if (!userRepository.existsById(dtoEntity.getId())) {
            throw new UpdateEntityException("not.created.validation");
        }
        if (userRepository.existsByLogin(dtoEntity.getLogin())) {
            throw new UpdateEntityException("unique.name.validation");
        }
        User user = userRepository.save(userDtoMapper.toEntity(dtoEntity));
        return userDtoMapper.toDto(user);
    }

    @Transactional
    @Override
    public UserDto save(UserDto dtoEntity) {
        if (userRepository.existsByLogin(dtoEntity.getLogin())) {
            throw new CreationEntityException("unique.name.validation");
        }
        dtoEntity.setPassword(passwordEncoder.encode(dtoEntity.getPassword()));
        dtoEntity.setRole(Role.CLIENT);
        User user = userRepository.save(userDtoMapper.toEntity(dtoEntity));
        return userDtoMapper.toDto(user);
    }

    @Override
    public EntityPageDto<UserDto> findAll(Map<String, String> params) {
        params = generalParamValidation.validateParams(params, ParameterValueList.USER_SORTING_FIELDS.getValues());
        Pageable pageable = generalService.getDefaultPageable(params);

        Page<User> findUsers;
        if (params.containsKey(SEARCH.name())) {
            String search = params.get(SEARCH.name());
            findUsers = userRepository.findDistinctByNameContainingOrSurnameContainingOrLoginContaining(search, search, search, pageable);
        } else {
            findUsers = userRepository.findAll(pageable);
        }

        if (findUsers.isEmpty()) {
            throw new EmptyResultException();
        }
        List<UserDto> users = findUsers.get()
                .map(userDtoMapper::toDto)
                .collect(Collectors.toList());
        return new EntityPageDto<>(users, (int) findUsers.getTotalElements());
    }

    @Override
    public UserDto findById(Integer id) {
        Optional<User> user = userRepository.findById(id);
        if (!user.isPresent()) {
            throw new EmptyResultException();
        }
        return userDtoMapper.toDto(user.get());
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (!userRepository.existsById(id)) {
            throw new DeleteEntityException("nothing.delete.validation");
        }
        userRepository.deleteById(id);
    }

    @Override
    public UserDto findByLogin(String login) {
        Optional<User> user = userRepository.findByLogin(login);
        if (!user.isPresent()) {
            throw new EmptyResultException();
        }
        return userDtoMapper.toDto(user.get());
    }

    @Override
    public UserDto findByLoginAndPassword(AuthRequestDto authRequestDto) {
        UserDto userDto;
        try {
            userDto = findByLogin(authRequestDto.getLogin());
        } catch (EmptyResultException e) {
            throw new InvalidCredentialsException();
        }
        if (!passwordEncoder.matches(authRequestDto.getPassword(), userDto.getPassword())) {
            throw new InvalidCredentialsException();
        }
        return userDto;
    }

    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        return UserDetailsImpl.fromUserDtoToCustomUserDetails(findByLogin(username));
    }
}