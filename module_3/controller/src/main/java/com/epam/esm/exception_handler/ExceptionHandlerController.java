package com.epam.esm.exception_handler;

import com.epam.esm.exception.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CreationEntityException.class)
    public ResponseEntity<ErrorMessage> handleCreationEntityException(CreationEntityException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.CREATION_ENTITY_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.CREATION_ENTITY_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UpdateEntityException.class)
    public ResponseEntity<ErrorMessage> handleUpdateEntityException(UpdateEntityException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.UPDATE_ENTITY_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.UPDATE_ENTITY_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DeleteEntityException.class)
    public ResponseEntity<ErrorMessage> handleDeleteEntityException(DeleteEntityException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.DELETE_ENTITY_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.DELETE_ENTITY_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationParamException.class)
    public ResponseEntity<ErrorMessage> handleValidationParamException(ValidationParamException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.VALIDATION_PARAM_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.VALIDATION_PARAM_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        StringBuilder sb = new StringBuilder();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = (error.getDefaultMessage() == null || error.getDefaultMessage().isEmpty())
                    ? ErrorResponse.VALIDATION_ENTITY_RESPONSE.getMessage()
                    : error.getDefaultMessage();
            sb.append(fieldName).append(": ").append(errorMessage).append("\n");
        });
        sb.deleteCharAt(sb.length() - 1);
        String message = sb.toString();
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.VALIDATION_ENTITY_RESPONSE.getCode(),
                (message.isEmpty())
                        ? ErrorResponse.VALIDATION_ENTITY_RESPONSE.getMessage()
                        : message),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorMessage> handleConstraintViolationException(ConstraintViolationException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.VALIDATION_PARAM_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.VALIDATION_PARAM_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmptyResultException.class)
    public ResponseEntity<ErrorMessage> handleEmptyResultException(EmptyResultException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.EMPTY_RESULT_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.EMPTY_RESULT_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(RepositoryException.class)
    public ResponseEntity<ErrorMessage> handleRepositoryException(RepositoryException ex) {
        return new ResponseEntity<>(new ErrorMessage(ErrorResponse.REPOSITORY_RESULT_RESPONSE.getCode(),
                (ex.getMessage() == null || ex.getMessage().isEmpty())
                        ? ErrorResponse.REPOSITORY_RESULT_RESPONSE.getMessage()
                        : ex.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}