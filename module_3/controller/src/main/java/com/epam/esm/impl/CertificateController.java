package com.epam.esm.impl;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.impl.certificate.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Map;

@RestController
@RequestMapping(value = "/certificates")
@Validated
public class CertificateController {

    private final CertificateService certificateService;

    @Autowired
    public CertificateController(CertificateService certificateService) {
        this.certificateService = certificateService;
    }

    @PostMapping
    public ResponseEntity<CertificateDto> save(@Valid @NotNull @RequestBody CertificateDto request) {
        return ResponseEntity.ok(certificateService.save(request));
    }

    @PutMapping
    public ResponseEntity<CertificateDto> update(@Valid @NotNull @RequestBody CertificateDto request) {
        return ResponseEntity.ok(certificateService.update(request));
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<CertificateDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        return ResponseEntity.ok(certificateService.findAll(params));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CertificateDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(certificateService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteById(@NotNull @Positive @PathVariable Integer id) {
        certificateService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}