package com.epam.esm.impl;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.user.UserDto;
import com.epam.esm.impl.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Map;

@RestController
@RequestMapping(value = "/users")
@Validated
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<UserDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        return ResponseEntity.ok(userService.findAll(params));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(userService.findById(id));
    }
}