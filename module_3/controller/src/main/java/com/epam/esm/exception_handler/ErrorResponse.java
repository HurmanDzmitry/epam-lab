package com.epam.esm.exception_handler;

public enum ErrorResponse {

    REPOSITORY_RESULT_RESPONSE(1004, "Repository error."),
    CREATION_ENTITY_RESPONSE(1005, "Entity creation error."),
    UPDATE_ENTITY_RESPONSE(1006, "Entity update error."),
    EMPTY_RESULT_RESPONSE(1007, "Empty result error."),
    VALIDATION_ENTITY_RESPONSE(1008, "Invalid entity error."),
    VALIDATION_PARAM_RESPONSE(1009, "Invalid param error."),
    DELETE_ENTITY_RESPONSE(1010, "Entity deletion error");

    private int code;
    private String message;

    ErrorResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
