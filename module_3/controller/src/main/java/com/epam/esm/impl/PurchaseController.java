package com.epam.esm.impl;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.purchase.PurchaseDto;
import com.epam.esm.impl.purchase.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Map;

@RestController
@RequestMapping(value = "/purchases")
@Validated
public class PurchaseController {

    private final PurchaseService purchaseService;

    @Autowired
    public PurchaseController(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }

    @GetMapping
    public ResponseEntity<EntityPageDto<PurchaseDto>> findAll(@NotNull @RequestParam Map<String, String> params) {
        return ResponseEntity.ok(purchaseService.findAll(params));
    }

    @GetMapping("/{id}")
    public ResponseEntity<PurchaseDto> findById(@NotNull @Positive @PathVariable Integer id) {
        return ResponseEntity.ok(purchaseService.findById(id));
    }

    @PostMapping
    public ResponseEntity<PurchaseDto> save(@NotNull @Positive @RequestParam Integer userId,
                                            @NotBlank @RequestParam String certificateIds) {
        return ResponseEntity.ok(purchaseService.save(new PurchaseDto(userId, certificateIds)));
    }
}