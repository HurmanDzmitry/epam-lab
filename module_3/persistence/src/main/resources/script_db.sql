CREATE TABLE user
(
  id       INT PRIMARY KEY AUTO_INCREMENT,
  name     VARCHAR(100) NOT NULL,
  surname  VARCHAR(100) NOT NULL,
  login    VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  birthday DATE         NOT NULL,
  role     ENUM ('admin', 'client')
);


CREATE TABLE tag
(
  id   INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL UNIQUE
);


CREATE TABLE certificate
(
  id               INT PRIMARY KEY AUTO_INCREMENT,
  name             VARCHAR(300) NOT NULL UNIQUE,
  price            DOUBLE       NOT NULL,
  create_date      VARCHAR(100) NOT NULL,
  last_update_date VARCHAR(100) NOT NULL,
  duration         INT          NOT NULL
);


CREATE TABLE certificates_has_tags
(
  id_tag         INT NOT NULL,
  id_certificate INT NOT NULL,
  FOREIGN KEY (id_tag) REFERENCES tag (id),
  FOREIGN KEY (id_certificate) REFERENCES certificate (id),
  UNIQUE (id_tag, id_certificate)
);


CREATE TABLE purchase
(
  id      INT PRIMARY KEY AUTO_INCREMENT,
  id_user INT    NOT NULL,
  date    DATE   NOT NULL,
  cost    DOUBLE NOT NULL,
  FOREIGN KEY (id_user) REFERENCES user (id)
);


CREATE TABLE purchase_has_certificates
(
  id               INT PRIMARY KEY AUTO_INCREMENT,
  id_purchase      INT    NOT NULL,
  id_certificate   INT    NOT NULL,
  cost_certificate DOUBLE NOT NULL DEFAULT -1,
  FOREIGN KEY (id_purchase) REFERENCES purchase (id),
  FOREIGN KEY (id_certificate) REFERENCES certificate (id)
);


INSERT INTO user
VALUES (1, 'Dima', 'Hurman', 'hurman', 'Qwerty12345', '1990-11-20', 1),
       (2, 'Serj', 'Tankian', 'tankian', '12345Abcd', '1980-09-20', 2),
       (3, 'Daron', 'Malakian', 'malakian', 'Abcd12345', '1985-07-20', 2),
       (4, 'Shavo', 'Odadjian', 'odadjian', 'Shavo777', '1980-05-20', 2),
       (5, 'John', 'Dolmayan', 'dolmayan', 'Dolmayan999', '1985-03-20', 2);


INSERT INTO tag
VALUES (1, 'beauty'),
       (2, 'entertainment'),
       (3, 'training'),
       (4, 'photoshoot'),
       (5, 'sport'),
       (6, 'quests'),
       (7, 'food'),
       (8, 'travels');

INSERT INTO certificate
VALUES (1, 'Online confectionery master class', 100,
        '2020-08-01T23:59:59.999-07:00', '2020-08-20T23:59:59.999+01:00', 50),
       (2, 'SPA rituals in Hammam', 200,
        '2020-08-02T23:59:59.999-06:00', '2020-08-21T23:59:59.999+02:00', 40),
       (3, 'Acting courses', 300,
        '2020-08-03T23:59:59.999-05:00', '2020-08-22T23:59:59.999+03:00', 30),
       (4, 'Swimming with a dolphin', 400,
        '2020-08-04T23:59:59.999-04:00', '2020-08-23T23:59:59.999+04:00', 20),
       (5, 'Course "Basics of drawing"', 500,
        '2020-08-05T23:59:59.999-03:00', '2020-08-24T23:59:59.999+05:00', 60),
       (6, 'Aroma care "Hot cherry"', 600,
        '2020-08-06T23:59:59.999-02:00', '2020-08-25T23:59:59.999+06:00', 70),
       (7, 'Two-level go-kart track', 700,
        '2020-08-07T23:59:59.999-01:00', '2020-08-26T23:59:59.999+07:00', 80),
       (8, 'Massage session', 800,
        '2020-08-08T23:59:59.999+00:00', '2020-08-27T23:59:59.999+08:00', 90),
       (9, 'Tea ceremony', 900,
        '2020-08-09T23:59:59.999+01:00', '2020-08-28T23:59:59.999+09:00', 100);


INSERT INTO certificates_has_tags
VALUES (7, 1),
       (3, 1),
       (1, 2),
       (3, 3),
       (8, 4),
       (2, 4),
       (3, 5),
       (1, 6),
       (2, 7),
       (5, 7),
       (1, 8),
       (7, 9),
       (3, 9),
       (2, 9);


INSERT INTO purchase
VALUES (1, 5, '2020-10-01', 900),
       (2, 2, '2020-10-10', 600),
       (3, 3, '2020-10-20', 100),
       (4, 4, '2020-10-30', 800);


INSERT INTO purchase_has_certificates
VALUES (1, 1, 4, 400),
       (2, 1, 5, 500),
       (3, 2, 3, 300),
       (4, 2, 3, 300),
       (5, 3, 1, 100),
       (6, 4, 8, 800)
