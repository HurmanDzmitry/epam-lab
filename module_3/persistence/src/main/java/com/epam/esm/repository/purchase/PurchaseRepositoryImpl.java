package com.epam.esm.repository.purchase;

import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Purchase;
import com.epam.esm.entity.User;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.repository.CriteriaGeneralTemplate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class PurchaseRepositoryImpl implements PurchaseRepository {

    private final SessionFactory sessionFactory;
    private final CriteriaGeneralTemplate criteriaGeneralTemplate;

    @Autowired
    public PurchaseRepositoryImpl(SessionFactory sessionFactory, CriteriaGeneralTemplate criteriaGeneralTemplate) {
        this.sessionFactory = sessionFactory;
        this.criteriaGeneralTemplate = criteriaGeneralTemplate;
    }

    @Override
    public EntityCount<Purchase> findAll(Map<String, String> params) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
            CriteriaQuery<Purchase> query = cb.createQuery(Purchase.class);
            Root<Purchase> root = query.from(Purchase.class);
            query.select(root);
            criteriaGeneralTemplate.getResultQuery(params, cb, query, root);
            if (params.containsKey("USER_ID")) {
                int userId = Integer.parseInt(params.get("USER_ID"));
                Join<Purchase, User> user = root.join("user", JoinType.INNER);
                query.where(cb.and(cb.equal(user.get("id"), userId)));
            }
            TypedQuery<Purchase> resultQuery = session.createQuery(query);
            List<Purchase> purchases = resultQuery.getResultList();
            int counts = purchases.size();
            criteriaGeneralTemplate.addPagination(params, resultQuery);
            purchases = purchases.stream().map(i -> getPurchaseWithCreationCost(i, session)).collect(Collectors.toList());
            return new EntityCount<>(purchases, counts);
        }
    }

    @Override
    public Purchase findById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Purchase purchase = session.find(Purchase.class, id);
            if (purchase == null) {
                return null;
            }
            return getPurchaseWithCreationCost(purchase, session);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            Purchase purchase = findById(id);
            session.remove(purchase);
            transaction.commit();
        }
    }

    @Override
    public Purchase save(Purchase entity) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            int id = (int) session.save(entity);
            List<Certificate> certificates = entity.getCertificates();
            certificates.forEach(c -> {
                NativeQuery query = session.createSQLQuery("UPDATE purchase_has_certificates " +
                        "SET cost_certificate = ? WHERE id_purchase = ? AND id_certificate = ?;");
                query.setParameter(1, c.getPrice());
                query.setParameter(2, id);
                query.setParameter(3, c.getId());
            });
            transaction.commit();
            return session.find(Purchase.class, id);
        }
    }

    @Override
    public Purchase update(Purchase entity) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            session.update(entity);
            transaction.commit();
            return session.find(Purchase.class, entity.getId());
        }
    }

    private Purchase getPurchaseWithCreationCost(Purchase purchase, Session session) {
        NativeQuery query = session.createSQLQuery("SELECT id_certificate, cost_certificate " +
                "FROM purchase_has_certificates " +
                "WHERE id_purchase = ?;");
        query.setParameter(1, purchase.getId());
        List<Object[]> resultList = query.getResultList();
        List<Certificate> certificates = resultList.stream().map(i -> {
            Certificate certificate = session.find(Certificate.class, i[0]);
            BigDecimal cost = BigDecimal.valueOf((double) i[1]);
            certificate.setPrice(cost);
            return certificate;
        }).collect(Collectors.toList());
        purchase.setCertificates(certificates);
        return purchase;
    }
}