package com.epam.esm.entity.custom_hibernate_type;

import com.epam.esm.entity.Role;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;
import org.hibernate.type.descriptor.java.ImmutableMutabilityPlan;

public class RoleEnumJavaDescriptor extends AbstractTypeDescriptor<Role> {

    private static final long serialVersionUID = 240932687458594662L;

    public static final RoleEnumJavaDescriptor INSTANCE = new RoleEnumJavaDescriptor();

    public RoleEnumJavaDescriptor() {
        super(Role.class, ImmutableMutabilityPlan.INSTANCE);
    }

    @Override
    public String toString(Role value) {
        return value.name().toLowerCase();
    }

    @Override
    public Role fromString(String string) {
        return Role.valueOf(string.toUpperCase());
    }

    @Override
    public <X> X unwrap(Role value, Class<X> type, WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (String.class.isAssignableFrom(type)) {
            return (X) value.name().toLowerCase();
        }
        throw unknownUnwrap(type);
    }

    @Override
    public <X> Role wrap(X value, WrapperOptions options) {
        if (value == null) {
            return null;
        }
        if (value instanceof String) {
            return Role.valueOf(((String) value).toUpperCase());
        }
        throw unknownWrap(value.getClass());
    }
}

