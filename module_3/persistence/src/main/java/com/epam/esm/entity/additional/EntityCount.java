package com.epam.esm.entity.additional;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Data
@ToString
public class EntityCount<T> {

    private List<T> entities;
    private int counts;

    public EntityCount(List<T> entities, int counts) {
        this.entities = entities;
        this.counts = counts;
    }
}
