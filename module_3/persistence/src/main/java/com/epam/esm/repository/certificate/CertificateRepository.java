package com.epam.esm.repository.certificate;

import com.epam.esm.entity.Certificate;
import com.epam.esm.repository.ApiRepository;

public interface CertificateRepository extends ApiRepository<Certificate, Integer> {

    Certificate findByName(String name);
}