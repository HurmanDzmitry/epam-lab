package com.epam.esm.entity.additional;

public enum Parameter {

    SORT_BY("id"),
    SORT_HOW("asc"),
    SEARCH(""),
    SEARCH_BY(""),
    PAGE("1"),
    PAGE_SIZE("10");

    private String defaultValue;

    Parameter(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
