package com.epam.esm.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = "user")
@ToString(exclude = "user")
public class Purchase implements Serializable {

    private static final long serialVersionUID = 2271077215375636008L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(updatable = false)
    private LocalDate date;

    @Column(updatable = false)
    private BigDecimal cost;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "purchase_has_certificates",
            joinColumns = {@JoinColumn(name = "id_purchase")},
            inverseJoinColumns = {@JoinColumn(name = "id_certificate")})
    private List<Certificate> certificates;

    public Purchase(LocalDate date, BigDecimal cost, User user, List<Certificate> certificates) {
        this.date = date;
        this.cost = cost;
        this.user = user;
        this.certificates = certificates;
    }
}
