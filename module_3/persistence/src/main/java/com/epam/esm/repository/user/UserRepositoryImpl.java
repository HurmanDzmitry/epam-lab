package com.epam.esm.repository.user;

import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.User;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.repository.CriteriaGeneralTemplate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;
    private final CriteriaGeneralTemplate criteriaGeneralTemplate;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory, CriteriaGeneralTemplate criteriaGeneralTemplate) {
        this.sessionFactory = sessionFactory;
        this.criteriaGeneralTemplate = criteriaGeneralTemplate;
    }

    @Override
    public EntityCount<User> findAll(Map<String, String> params) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
            CriteriaQuery<User> query = cb.createQuery(User.class);
            Root<User> root = query.from(User.class);
            query.select(root);
            criteriaGeneralTemplate.getResultQuery(params, cb, query, root);
            TypedQuery<User> resultQuery = session.createQuery(query);
            int counts = resultQuery.getResultList().size();
            criteriaGeneralTemplate.addPagination(params, resultQuery);
            List<User> users = resultQuery.getResultList();
            return new EntityCount<>(users, counts);
        }
    }

    @Override
    public User findById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(User.class, id);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            User user = findById(id);
            session.remove(user);
            transaction.commit();
        }
    }

    @Override
    public User save(User entity) {
        try (Session session = sessionFactory.openSession()) {
            int id = (int) session.save(entity);
            return session.find(User.class, id);
        }
    }

    @Override
    public User update(User entity) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            session.update(entity);
            transaction.commit();
            return session.find(User.class, entity.getId());
        }
    }

    @Override
    public User findByLogin(String login) {
        try (Session session = sessionFactory.openSession()) {
            TypedQuery<User> q = session.createQuery("select c from User c where c.name = :login",
                    User.class);
            q.setParameter("login", login);
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}