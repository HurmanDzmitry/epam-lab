package com.epam.esm.repository;

import com.epam.esm.entity.additional.EntityCount;

import java.util.Map;

public interface ApiRepository<T, K> {

    T save(T entity);

    T update(T entity);

    EntityCount<T> findAll(Map<String, String> params);

    T findById(K id);

    void deleteById(K id);
}