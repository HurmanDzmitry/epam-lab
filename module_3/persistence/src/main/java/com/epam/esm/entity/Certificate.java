package com.epam.esm.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Set;

@Entity
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(exclude = "purchases")
@ToString(exclude = "purchases")
public class Certificate implements Serializable {

    private static final long serialVersionUID = -9023362695285929629L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private BigDecimal price;

    @Column(name = "create_date", updatable = false)
    @Type(type = "com.epam.esm.entity.custom_hibernate_type.OffsetDateTimeStringType")
    private OffsetDateTime createDate;

    @Column(name = "last_update_date")
    @Type(type = "com.epam.esm.entity.custom_hibernate_type.OffsetDateTimeStringType")
    private OffsetDateTime lastUpdateDate;

    private int duration;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "certificates_has_tags",
            joinColumns = {@JoinColumn(name = "id_certificate")},
            inverseJoinColumns = {@JoinColumn(name = "id_tag")})
    private Set<Tag> tags;

    @ManyToMany(mappedBy = "certificates")
    private Set<Purchase> purchases;
}