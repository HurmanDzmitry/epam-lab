package com.epam.esm.entity.additional;

import java.util.Arrays;
import java.util.List;

public enum ParameterValueList {

    SORTING_DIRECTION(Arrays.asList(Parameter.SORT_HOW.getDefaultValue(), "desc")),

    TAG_SORTING_FIELDS(Arrays.asList("id", "name")),
    CERTIFICATE_SORTING_FIELDS(Arrays.asList("id", "name", "price", "create_date", "last_update_date", "duration")),
    USER_SORTING_FIELDS(Arrays.asList("id", "name", "surname", "login", "password", "birthday", "role")),
    PURCHASE_SORTING_FIELDS(Arrays.asList("id", "id_user", "id_certificate", "date", "cost")),

    TAG_SEARCHING_FIELDS(Arrays.asList("name")),
    CERTIFICATE_SEARCHING_FIELDS(Arrays.asList("name")),
    USER_SEARCHING_FIELDS(Arrays.asList("name", "surname", "login")),
    PURCHASE_SEARCHING_FIELDS(Arrays.asList(""));

    private List<String> values;

    ParameterValueList(List<String> values) {
        this.values = values;
    }

    public List<String> getValues() {
        return values;
    }
}
