package com.epam.esm.repository.tag;

import com.epam.esm.entity.Tag;
import com.epam.esm.repository.ApiRepository;

public interface TagRepository extends ApiRepository<Tag, Integer> {

    Tag findByName(String name);

    Tag findMostExpensiveUserTag(Integer userId);
}