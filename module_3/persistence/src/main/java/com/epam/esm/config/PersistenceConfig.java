package com.epam.esm.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import java.util.Properties;

@Configuration
public class PersistenceConfig {

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String dialectProperty;
    @Value("${spring.jpa.properties.hibernate.current_session_context_class}")
    private String sessionContextClassProperty;
    @Value("${spring.jpa.properties.hibernate.jdbc.time_zone}")
    private String timeZoneProperty;
    @Value("${spring.jpa.properties.hibernate.ddl-auto}")
    private String ddlProperty;

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setPackagesToScan("com.epam.esm");
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setHibernateProperties(getProperties());
        return sessionFactory;
    }

    @Bean
    public HikariDataSource dataSource() {
        HikariConfig config = new HikariConfig();
        HikariDataSource dataSource;
        config.setDriverClassName(driverClassName);
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        dataSource = new HikariDataSource(config);
        return dataSource;
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", dialectProperty);
        properties.setProperty("hibernate.current_session_context_class", sessionContextClassProperty);
        properties.setProperty("hibernate.jdbc.time_zone", timeZoneProperty);
        properties.setProperty("hibernate.ddl", ddlProperty);
        return properties;
    }
}