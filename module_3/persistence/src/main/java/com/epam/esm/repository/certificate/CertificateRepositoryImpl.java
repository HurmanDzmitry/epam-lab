package com.epam.esm.repository.certificate;

import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.repository.CriteriaGeneralTemplate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class CertificateRepositoryImpl implements CertificateRepository {

    private final SessionFactory sessionFactory;
    private final CriteriaGeneralTemplate criteriaGeneralTemplate;

    @Autowired
    public CertificateRepositoryImpl(SessionFactory sessionFactory, CriteriaGeneralTemplate criteriaGeneralTemplate) {
        this.sessionFactory = sessionFactory;
        this.criteriaGeneralTemplate = criteriaGeneralTemplate;
    }

    @Override
    public EntityCount<Certificate> findAll(Map<String, String> params) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
            CriteriaQuery<Certificate> query = cb.createQuery(Certificate.class);
            Root<Certificate> root = query.from(Certificate.class);
            query.select(root);
            criteriaGeneralTemplate.getResultQuery(params, cb, query, root);
            if (params.containsKey("TAG_NAME")) {
                String tagNames = params.get("TAG_NAME");
                Join<Certificate, Tag> tag = root.join("tags", JoinType.INNER);
                Arrays.stream(tagNames.split(",")).forEach(name -> query.where(cb.and(cb.equal(tag.get("name"), name))));
            }
            TypedQuery<Certificate> resultQuery = session.createQuery(query);
            int counts = resultQuery.getResultList().size();
            criteriaGeneralTemplate.addPagination(params, resultQuery);
            List<Certificate> certificates = resultQuery.getResultList();
            return new EntityCount<>(certificates, counts);
        }
    }

    @Override
    public Certificate findById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(Certificate.class, id);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            Certificate certificate = findById(id);
            session.remove(certificate);
            transaction.commit();
        }
    }

    @Override
    public Certificate save(Certificate entity) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            int id = (int) session.save(entity);
            transaction.commit();
            return session.find(Certificate.class, id);
        }
    }

    @Override
    public Certificate update(Certificate entity) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            session.update(entity);
            transaction.commit();
            return session.find(Certificate.class, entity.getId());
        }
    }

    @Override
    public Certificate findByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            TypedQuery<Certificate> q = session.createQuery("select c from Certificate c where c.name = :name",
                    Certificate.class);
            q.setParameter("name", name);
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}