package com.epam.esm.repository.tag;

import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.exception.RepositoryException;
import com.epam.esm.repository.CriteriaGeneralTemplate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;

@Repository
public class TagRepositoryImpl implements TagRepository {

    private final SessionFactory sessionFactory;
    private final CriteriaGeneralTemplate criteriaGeneralTemplate;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory, CriteriaGeneralTemplate criteriaGeneralTemplate) {
        this.sessionFactory = sessionFactory;
        this.criteriaGeneralTemplate = criteriaGeneralTemplate;
    }

    @Override
    public EntityCount<Tag> findAll(Map<String, String> params) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder cb = sessionFactory.getCriteriaBuilder();
            CriteriaQuery<Tag> query = cb.createQuery(Tag.class);
            Root<Tag> root = query.from(Tag.class);
            query.select(root);
            criteriaGeneralTemplate.getResultQuery(params, cb, query, root);
            TypedQuery<Tag> resultQuery = session.createQuery(query);
            int counts = resultQuery.getResultList().size();
            criteriaGeneralTemplate.addPagination(params, resultQuery);
            List<Tag> tags = resultQuery.getResultList();
            return new EntityCount<>(tags, counts);
        }
    }

    @Override
    public Tag findById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            return session.find(Tag.class, id);
        }
    }

    @Override
    public void deleteById(Integer id) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            NativeQuery sqlQuery = session.createSQLQuery("select count(id_certificate) " +
                    "from certificates_has_tags " +
                    "join certificate c on certificates_has_tags.id_certificate = c.id " +
                    "group by id_certificate ");
            int countsBefore = sqlQuery.getResultList().size();
            session.createSQLQuery("delete from certificates_has_tags where id_tag=" + id);
            int countsAfter = sqlQuery.getResultList().size();
            if (countsBefore != countsAfter) {
                transaction.rollback();
                throw new RepositoryException("The deletion did not go through, the relationship of entities is broken. " +
                        "Perhaps the tag to be removed is the last one on some certificate.");
            }
            Tag tag = findById(id);
            session.remove(tag);
            transaction.commit();
        }
    }

    @Override
    public Tag save(Tag entity) {
        try (Session session = sessionFactory.openSession()) {
            int id = (int) session.save(entity);
            return session.find(Tag.class, id);
        }
    }

    @Override
    public Tag update(Tag entity) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            session.update(entity);
            transaction.commit();
            return session.find(Tag.class, entity.getId());
        }
    }

    @Override
    public Tag findByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            TypedQuery<Tag> q = session.createQuery("select t from Tag t where t.name = :name", Tag.class);
            q.setParameter("name", name);
            return q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Tag findMostExpensiveUserTag(Integer userId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery query = session.createSQLQuery("select tag.id " +
                    "from tag " +
                    "       join certificates_has_tags cht on tag.id = cht.id_tag " +
                    "       join certificate c on cht.id_certificate = c.id " +
                    "       join purchase_has_certificates phc on c.id = phc.id_certificate " +
                    "       join purchase p on phc.id_purchase = p.id " +
                    "       join user u on p.id_user = u.id " +
                    "where u.id = ? " +
                    "group by tag.name " +
                    "order by sum(p.cost) desc " +
                    "limit 1;");
            query.setParameter(1, userId);
            int tagId = (int) query.getSingleResult();
            return findById(tagId);
        } catch (NoResultException e) {
            return null;
        }
    }
}