package com.epam.esm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "purchase_has_certificates")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PurchaseCertificate implements Serializable {

    private static final long serialVersionUID = 4029083195524495321L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "cost_certificate", updatable = false, scale = 2)
    private BigDecimal cost;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_purchase")
    private Purchase purchase;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_certificate")
    private Certificate certificate;
}
