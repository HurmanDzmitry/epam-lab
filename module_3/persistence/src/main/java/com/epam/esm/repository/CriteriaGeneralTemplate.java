package com.epam.esm.repository;

import com.epam.esm.entity.additional.ParameterValueList;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.epam.esm.entity.additional.Parameter.*;

@Component
public class CriteriaGeneralTemplate<T> {

    private static final String SEPARATOR = " ";
    private static final String WILDCARD = "%";

    public CriteriaQuery getResultQuery(Map<String, String> params, CriteriaBuilder cb, CriteriaQuery query,
                                        Root root) {
        query.select(root);
        addSearching(params, cb, query, root);
        addSorting(params, cb, query, root);
        return query;
    }

    public void addPagination(Map<String, String> params, TypedQuery query) {
        int pageSize = Integer.parseInt(params.get(PAGE_SIZE.name()));
        int page = Integer.parseInt(params.get(PAGE.name()));
        query.setMaxResults(pageSize);
        query.setFirstResult((page - 1) * pageSize);
    }

    private void addSearching(Map<String, String> params, CriteriaBuilder cb, CriteriaQuery query, Root root) {
        String search = params.get(SEARCH.name());
        if (!search.isEmpty()) {
            String searchBy = params.get(SEARCH_BY.name());
            List<String> searchBys = searchBy.isEmpty()
                    ? ParameterValueList.TAG_SEARCHING_FIELDS.getValues()
                    : Arrays.asList(searchBy.split(SEPARATOR));
            searchBys.forEach(value -> query.where(cb.or(cb.like(root.get(value), WILDCARD + search + WILDCARD))));
        }
    }

    private void addSorting(Map<String, String> params, CriteriaBuilder cb, CriteriaQuery query, Root root) {
        String sortHow = params.get(SORT_HOW.name());
        String sortBy = params.get(SORT_BY.name());
        if (sortHow.equals(SORT_HOW.getDefaultValue())) {
            query.orderBy(cb.asc(root.get(sortBy)));
        } else {
            query.orderBy(cb.desc(root.get(sortBy)));
        }
    }
}