package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class RepositoryException extends RuntimeException {

    private static final long serialVersionUID = -1725198864118242188L;

    public RepositoryException() {
    }

    public RepositoryException(String message) {
        super(message);
    }

    public RepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepositoryException(Throwable cause) {
        super(cause);
    }
}

