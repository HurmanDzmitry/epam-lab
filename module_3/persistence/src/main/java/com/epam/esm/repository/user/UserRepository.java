package com.epam.esm.repository.user;

import com.epam.esm.entity.User;
import com.epam.esm.repository.ApiRepository;

public interface UserRepository extends ApiRepository<User, Integer> {

    User findByLogin(String login);
}
