package com.epam.esm.entity.custom_hibernate_type;

import com.epam.esm.entity.Role;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

public class RoleEnumType extends AbstractSingleColumnStandardBasicType<Role> {

    private static final long serialVersionUID = -7648084570100933944L;

    public static final RoleEnumType INSTANCE = new RoleEnumType();

    public RoleEnumType() {
        super(VarcharTypeDescriptor.INSTANCE, RoleEnumJavaDescriptor.INSTANCE);
    }

    @Override
    public String getName() {
        return "RoleEnum";
    }
}

