package com.epam.esm.repository.purchase;

import com.epam.esm.entity.Purchase;
import com.epam.esm.repository.ApiRepository;

public interface PurchaseRepository extends ApiRepository<Purchase, Integer> {
}