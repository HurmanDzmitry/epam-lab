package com.epam.esm.dto.purchase;

import com.epam.esm.dto.certificate.CertificateDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@Data
@ToString
public class PurchaseDto implements Serializable {

    private static final long serialVersionUID = -5635213115339907113L;

    private int id;

    @NotNull(message = "shouldn't be null.")
    @PastOrPresent(message = "should be in the past or in the present.")
    private LocalDate date;

    @NotNull(message = "shouldn't be null.")
    @DecimalMin(value = "0", message = "should be {value} or more.")
    private BigDecimal cost;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotNull(message = "shouldn't be null.")
    @Positive(message = "should be zero or more.")
    private int userId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String certificateIds;

    @NotNull(message = "shouldn't be null.")
    @Positive(message = "should be zero or more.")
    private List<CertificateDto> certificates;

    public PurchaseDto(int userId, String certificateIds) {
        this.userId = userId;
        this.certificateIds = certificateIds;
    }
}
