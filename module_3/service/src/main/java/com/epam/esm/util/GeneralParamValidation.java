package com.epam.esm.util;

import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.ValidationParamException;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.epam.esm.entity.additional.Parameter.*;

public class GeneralParamValidation {

    private static Map<String, String> inputParams;

    private static final String SEPARATOR = " ";
    private static final int MIN_COUNT_IN_ONE_PAGE = 1;

    public static Map<String, String> validate(Map<String, String> params, List<String> validSortingValues,
                                               List<String> validSearchingValues) {
        inputParams = params.entrySet()
                .stream()
                .collect(Collectors.toMap(
                        i -> i.getKey().toUpperCase(),
                        i -> i.getValue().toLowerCase()));
        sortByValueValidate(validSortingValues);
        sortHowValueValidate();
        searchValuesValidate(validSearchingValues);
        paginationValidate();

        return inputParams;
    }

    private static void sortByValueValidate(List<String> validSortingValues) {
        String param = primaryValidate(SORT_BY.name(), SORT_BY.getDefaultValue());
        if (!validSortingValues.contains(param)) {
            throw new ValidationParamException(SORT_BY.name() + " does not match the list of valid values.");
        }
    }

    private static void sortHowValueValidate() {
        String param = primaryValidate(SORT_HOW.name(), SORT_HOW.getDefaultValue());
        if (!ParameterValueList.SORTING_DIRECTION.getValues().contains(param)) {
            throw new ValidationParamException(SORT_HOW.name() + " does not match the list of valid values.");
        }
    }

    private static void searchValuesValidate(List<String> validSearchingValues) {
        primaryValidate(SEARCH.name(), SEARCH.getDefaultValue());

        String param = primaryValidate(SEARCH_BY.name(), SEARCH_BY.getDefaultValue());
        if (!param.equals(SEARCH_BY.getDefaultValue())
                && !Arrays.stream(param.split(SEPARATOR)).allMatch(validSearchingValues::contains)) {
            throw new ValidationParamException(SEARCH_BY.name() + " does not match the list of valid values.");
        }
    }

    private static void paginationValidate() {
        String param = primaryValidate(PAGE_SIZE.name(), PAGE_SIZE.getDefaultValue());
        int pageSize;
        try {
            pageSize = Integer.parseInt(param);
            if (pageSize < MIN_COUNT_IN_ONE_PAGE) {
                throw new ValidationParamException(PAGE_SIZE.name() + " is less than need border: "
                        + MIN_COUNT_IN_ONE_PAGE + ".");
            }
        } catch (NumberFormatException e) {
            throw new ValidationParamException(PAGE_SIZE.name() + " is not an integer.");
        }

        param = primaryValidate(PAGE.name(), PAGE.getDefaultValue());
        try {
            int page = Integer.parseInt(param);
            if (page <= 0) {
                throw new ValidationParamException(PAGE.name() + " is zero or not positive integer.");
            }
        } catch (NumberFormatException e) {
            throw new ValidationParamException(PAGE.name() + " is not an integer.");
        }
    }

    private static String primaryValidate(String param, String defaultValue) {
        String value;
        if (inputParams.containsKey(param)) {
            value = inputParams.get(param);
            if (value == null || value.trim().isEmpty()) {
                throw new ValidationParamException("Parameter: " + param + ", value: "
                        + (value == null ? "null" : "empty"));
            }
        } else {
            value = defaultValue;
        }
        inputParams.put(param, value);
        return inputParams.get(param);
    }
}
