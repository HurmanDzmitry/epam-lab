package com.epam.esm.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@ToString
public class EntityPageDto<T> {

    @NotNull(message = "shouldn't be null.")
    private List<T> entities;

    @NotNull(message = "shouldn't be null.")
    @Positive(message = "should be positive integer.")
    private int totalCounts;
}
