package com.epam.esm.impl.tag;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.tag.TagDto;
import com.epam.esm.dto.tag.TagDtoMapper;
import com.epam.esm.entity.Tag;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.entity.additional.Parameter;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.*;
import com.epam.esm.repository.tag.TagRepository;
import com.epam.esm.util.GeneralParamValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class TagServiceImpl implements TagService {

    private final TagRepository tagDao;
    private final TagDtoMapper tagDtoMapper;

    @Autowired
    public TagServiceImpl(TagRepository tagDao, TagDtoMapper tagDtoMapper) {
        this.tagDao = tagDao;
        this.tagDtoMapper = tagDtoMapper;
    }

    @Transactional
    @Override
    public TagDto update(TagDto dtoEntity) {
        if (dtoEntity.getId() <= 0) {
            throw new ValidationParamException("Entity id is zero or not positive integer.");
        }
        Tag tag = tagDao.findById(dtoEntity.getId());
        if (tag == null) {
            throw new UpdateEntityException("This entity has not yet been created.");
        }
        tag = tagDao.findByName(dtoEntity.getName());
        if (tag != null) {
            throw new UpdateEntityException("The uniqueness of the entity name is violated. " +
                    "An entity with name this already exists.");
        }
        tag = tagDao.update(tagDtoMapper.toEntity(dtoEntity));
        return tagDtoMapper.toDto(tag);
    }

    @Transactional
    @Override
    public TagDto save(TagDto dtoEntity) {
        Tag tag = tagDao.findById(dtoEntity.getId());
        if (tag != null) {
            throw new RepositoryException("Entity has already been created with this id.");
        }
        tag = tagDao.findByName(dtoEntity.getName());
        if (tag != null) {
            throw new RepositoryException("The uniqueness of the entity name is violated. " +
                    "An entity with this name this already exists.");
        }
        tag = tagDao.save(tagDtoMapper.toEntity(dtoEntity));
        return tagDtoMapper.toDto(tag);
    }

    @Override
    public EntityPageDto<TagDto> findAll(Map<String, String> params) {
        params = GeneralParamValidation.validate(params, ParameterValueList.TAG_SORTING_FIELDS.getValues(),
                ParameterValueList.TAG_SEARCHING_FIELDS.getValues());
        EntityCount<Tag> tagEntityCount = tagDao.findAll(params);
        List<TagDto> tags = tagEntityCount.getEntities()
                .stream()
                .map(tagDtoMapper::toDto)
                .collect(Collectors.toList());
        if (tags.isEmpty()) {
            throw new EmptyResultException();
        }
        int counts = tagEntityCount.getCounts();
        int pageSize = Integer.parseInt(params.get(Parameter.PAGE_SIZE.name()));
        int page = Integer.parseInt(params.get(Parameter.PAGE.name()));
        int totalPages = (int) (Math.ceil((double) counts / pageSize));
        if (totalPages < page) {
            throw new ValidationParamException(Parameter.PAGE.name() + " is more than possible: " + totalPages + ".");
        }
        return new EntityPageDto<>(tags, counts);
    }

    @Override
    public TagDto findById(Integer id) {
        Tag tag = tagDao.findById(id);
        if (tag == null) {
            throw new EmptyResultException();
        }
        return tagDtoMapper.toDto(tag);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (tagDao.findById(id) == null) {
            throw new DeleteEntityException("There is no entity with this identifier. There is nothing to delete.");
        }
        tagDao.deleteById(id);
    }

    @Override
    public TagDto findMostExpensiveUserTag(Integer id) {
        Tag tag = tagDao.findMostExpensiveUserTag(id);
        if (tag == null) {
            throw new EmptyResultException();
        }
        return tagDtoMapper.toDto(tag);
    }
}
