package com.epam.esm.impl.user;

import com.epam.esm.ApiService;
import com.epam.esm.dto.user.UserDto;

public interface UserService extends ApiService<UserDto, Integer> {
}
