package com.epam.esm.impl.user;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.user.UserDto;
import com.epam.esm.dto.user.UserDtoMapper;
import com.epam.esm.entity.User;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.entity.additional.Parameter;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.*;
import com.epam.esm.repository.user.UserRepository;
import com.epam.esm.util.GeneralParamValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userDao;
    private final UserDtoMapper userDtoMapper;

    @Autowired
    public UserServiceImpl(UserRepository userDao, UserDtoMapper userDtoMapper) {
        this.userDao = userDao;
        this.userDtoMapper = userDtoMapper;
    }

    @Transactional
    @Override
    public UserDto update(UserDto dtoEntity) {
        if (dtoEntity.getId() <= 0) {
            throw new ValidationParamException("Entity id is zero or not positive integer.");
        }
        User user = userDao.findById(dtoEntity.getId());
        if (user == null) {
            throw new UpdateEntityException("This entity has not yet been created.");
        }
        user = userDao.findByLogin(dtoEntity.getLogin());
        if (user != null) {
            throw new RepositoryException("The uniqueness of the entity name is violated. " +
                    "An entity with this login this already exists.");
        }
        user = userDao.update(userDtoMapper.toEntity(dtoEntity));
        return userDtoMapper.toDto(user);
    }

    @Transactional
    @Override
    public UserDto save(UserDto dtoEntity) {
        User user = userDao.findById(dtoEntity.getId());
        if (user != null) {
            throw new RepositoryException("Entity has already been created with this id.");
        }
        user = userDao.findByLogin(dtoEntity.getLogin());
        if (user != null) {
            throw new RepositoryException("The uniqueness of the entity name is violated. " +
                    "An entity with this login this already exists.");
        }
        user = userDao.save(userDtoMapper.toEntity(dtoEntity));
        return userDtoMapper.toDto(user);
    }

    @Override
    public EntityPageDto<UserDto> findAll(Map<String, String> params) {
        params = GeneralParamValidation.validate(params, ParameterValueList.USER_SORTING_FIELDS.getValues(),
                ParameterValueList.USER_SEARCHING_FIELDS.getValues());
        EntityCount<User> userEntityCount = userDao.findAll(params);
        List<UserDto> users = userEntityCount.getEntities()
                .stream()
                .map(userDtoMapper::toDto)
                .collect(Collectors.toList());
        if (users.isEmpty()) {
            throw new EmptyResultException();
        }
        int counts = userEntityCount.getCounts();
        int pageSize = Integer.parseInt(params.get(Parameter.PAGE_SIZE.name()));
        int page = Integer.parseInt(params.get(Parameter.PAGE.name()));
        int totalPages = (int) (Math.ceil((double) counts / pageSize));
        if (totalPages < page) {
            throw new ValidationParamException(Parameter.PAGE.name() + " is more than possible: " + totalPages + ".");
        }
        return new EntityPageDto<>(users, counts);
    }

    @Override
    public UserDto findById(Integer id) {
        User user = userDao.findById(id);
        if (user == null) {
            throw new EmptyResultException();
        }
        return userDtoMapper.toDto(user);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (userDao.findById(id) == null) {
            throw new DeleteEntityException("There is no entity with this identifier. There is nothing to delete.");
        }
        userDao.deleteById(id);
    }
}
