package com.epam.esm.impl.certificate;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.certificate.CertificateDto;
import com.epam.esm.dto.certificate.CertificateDtoMapper;
import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.entity.additional.Parameter;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.*;
import com.epam.esm.repository.certificate.CertificateRepository;
import com.epam.esm.util.GeneralParamValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CertificateServiceImpl implements CertificateService {

    private final CertificateRepository certificateDao;
    private final CertificateDtoMapper certificateDtoMapper;

    @Autowired
    public CertificateServiceImpl(CertificateRepository certificateDao, CertificateDtoMapper certificateDtoMapper) {
        this.certificateDao = certificateDao;
        this.certificateDtoMapper = certificateDtoMapper;
    }

    @Transactional
    @Override
    public CertificateDto update(CertificateDto dtoEntity) {
        if (dtoEntity.getId() <= 0) {
            throw new ValidationParamException("Entity id is zero or not positive integer.");
        }
        Certificate certificate = certificateDao.findById(dtoEntity.getId());
        if (certificate == null) {
            throw new UpdateEntityException("This entity has not yet been created.");
        }
        OffsetDateTime createDate = certificate.getCreateDate();
        certificate = certificateDao.findByName(dtoEntity.getName());
        if (certificate != null) {
            throw new UpdateEntityException("The uniqueness of the entity name is violated. " +
                    "An entity with name this already exists.");
        }
        certificate = certificateDtoMapper.toEntity(dtoEntity);
        certificate.setCreateDate(createDate);
        certificate.setLastUpdateDate(OffsetDateTime.now());
        certificate = certificateDao.update(certificate);
        return certificateDtoMapper.toDto(certificate);
    }

    @Transactional
    @Override
    public CertificateDto save(CertificateDto dtoEntity) {
        Certificate certificate = certificateDao.findById(dtoEntity.getId());
        if (certificate != null) {
            throw new RepositoryException("Entity has already been created with this id.");
        }
        certificate = certificateDao.findByName(dtoEntity.getName());
        if (certificate != null) {
            throw new RepositoryException("The uniqueness of the entity name is violated. " +
                    "An entity with this name this already exists.");
        }
        certificate = certificateDtoMapper.toEntity(dtoEntity);
        certificate.setCreateDate(OffsetDateTime.now());
        certificate.setLastUpdateDate(OffsetDateTime.now());
        certificate = certificateDao.save(certificate);
        return certificateDtoMapper.toDto(certificate);
    }

    @Override
    public EntityPageDto<CertificateDto> findAll(Map<String, String> params) {
        params = GeneralParamValidation.validate(params, ParameterValueList.CERTIFICATE_SORTING_FIELDS.getValues(),
                ParameterValueList.CERTIFICATE_SEARCHING_FIELDS.getValues());
        EntityCount<Certificate> certificateEntityCount = certificateDao.findAll(params);
        List<CertificateDto> certificates = certificateEntityCount.getEntities()
                .stream()
                .map(certificateDtoMapper::toDto)
                .collect(Collectors.toList());
        if (certificates.isEmpty()) {
            throw new EmptyResultException();
        }
        int counts = certificateEntityCount.getCounts();
        int pageSize = Integer.parseInt(params.get(Parameter.PAGE_SIZE.name()));
        int page = Integer.parseInt(params.get(Parameter.PAGE.name()));
        int totalPages = (int) (Math.ceil((double) counts / pageSize));
        if (totalPages < page) {
            throw new ValidationParamException(Parameter.PAGE.name() + " is more than possible: " + totalPages + ".");
        }
        return new EntityPageDto<>(certificates, counts);
    }

    @Override
    public CertificateDto findById(Integer id) {
        Certificate certificate = certificateDao.findById(id);
        if (certificate == null) {
            throw new EmptyResultException();
        }
        return certificateDtoMapper.toDto(certificate);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (certificateDao.findById(id) == null) {
            throw new DeleteEntityException("There is no entity with this identifier. There is nothing to delete.");
        }
        certificateDao.deleteById(id);
    }
}