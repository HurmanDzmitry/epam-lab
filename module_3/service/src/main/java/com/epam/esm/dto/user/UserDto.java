package com.epam.esm.dto.user;

import com.epam.esm.dto.purchase.PurchaseDto;
import com.epam.esm.entity.Role;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@Data
@ToString
public class UserDto implements Serializable {

    private static final long serialVersionUID = -6673912807420352406L;

    private int id;

    @NotBlank(message = "shouldn't be null and empty.")
    @Size(min = 3, max = 100, message = "should be min={min} and max={max}.")
    private String name;

    @NotBlank(message = "shouldn't be null and empty.")
    @Size(min = 3, max = 100, message = "should be min={min} and max={max}.")
    private String surname;

    @NotBlank(message = "shouldn't be null and empty.")
    @Size(min = 3, max = 100, message = "should be min={min} and max={max}.")
    private String login;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @NotBlank(message = "shouldn't be null and empty.")
    @Pattern(regexp = "^(?=.*[\\d])(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=\\S+$).{8,100}$",
            message = "must be larger than 8 but less than 100, must contain upper and lower case letters, " +
                    "numbers and special characters.")
    private String password;

    @NotNull(message = "shouldn't be null.")
    @Past(message = "should be in the past.")
    private LocalDate birthday;

    @NotNull(message = "shouldn't be null.")
    private Role role;

    @NotNull(message = "shouldn't be null.")
    @NotEmpty(message = "shouldn't be empty.")
    private Set<@Valid @NotNull(message = "shouldn't be null.") PurchaseDto> purchases;
}
