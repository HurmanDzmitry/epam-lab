package com.epam.esm.impl.purchase;

import com.epam.esm.dto.EntityPageDto;
import com.epam.esm.dto.purchase.PurchaseDto;
import com.epam.esm.dto.purchase.PurchaseDtoMapper;
import com.epam.esm.entity.Certificate;
import com.epam.esm.entity.Purchase;
import com.epam.esm.entity.User;
import com.epam.esm.entity.additional.EntityCount;
import com.epam.esm.entity.additional.Parameter;
import com.epam.esm.entity.additional.ParameterValueList;
import com.epam.esm.exception.DeleteEntityException;
import com.epam.esm.exception.EmptyResultException;
import com.epam.esm.exception.RepositoryException;
import com.epam.esm.exception.ValidationParamException;
import com.epam.esm.repository.certificate.CertificateRepository;
import com.epam.esm.repository.purchase.PurchaseRepository;
import com.epam.esm.repository.user.UserRepository;
import com.epam.esm.util.GeneralParamValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    private final PurchaseRepository purchaseDao;
    private final UserRepository userRepository;
    private final CertificateRepository certificateRepository;
    private final PurchaseDtoMapper purchaseDtoMapper;

    @Autowired
    public PurchaseServiceImpl(PurchaseRepository purchaseDao, UserRepository userRepository,
                               CertificateRepository certificateRepository, PurchaseDtoMapper purchaseDtoMapper) {
        this.purchaseDao = purchaseDao;
        this.userRepository = userRepository;
        this.certificateRepository = certificateRepository;
        this.purchaseDtoMapper = purchaseDtoMapper;
    }

    @Transactional
    @Override
    public PurchaseDto update(PurchaseDto dtoEntity) {
        String[] split = dtoEntity.getCertificateIds().split(",");
        List<Integer> certificateIds;
        try {
            certificateIds = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        } catch (NumberFormatException e) {
            throw new ValidationParamException("Input parameters is not an integer.");
        }
        if (dtoEntity.getId() <= 0) {
            throw new ValidationParamException("Entity id is zero or not positive integer.");
        }
        Purchase purchase = purchaseDao.findById(dtoEntity.getId());
        if (purchase != null) {
            throw new RepositoryException("Entity has already been created with this id.");
        }
        User user = userRepository.findById(dtoEntity.getUserId());
        if (user == null) {
            throw new RepositoryException("User has not yet been created.");
        }
        List<Certificate> certificates = certificateIds.stream().map(id -> {
            Certificate certificate = certificateRepository.findById(id);
            if (certificate == null) {
                throw new RepositoryException("Certificate has not yet been created.");
            }
            return certificate;
        }).collect(Collectors.toList());
        BigDecimal cost = certificates.stream().map(Certificate::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        purchase = new Purchase(LocalDate.now(), cost, user, certificates);
        purchase = purchaseDao.update(purchase);
        return purchaseDtoMapper.toDto(purchase);
    }

    @Transactional
    @Override
    public PurchaseDto save(PurchaseDto dtoEntity) {
        String[] split = dtoEntity.getCertificateIds().split(",");
        List<Integer> certificateIds;
        try {
            certificateIds = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        } catch (NumberFormatException e) {
            throw new ValidationParamException("Input parameters is not an integer.");
        }
        Purchase purchase = purchaseDao.findById(dtoEntity.getId());
        if (purchase != null) {
            throw new RepositoryException("Entity has already been created with this id.");
        }
        User user = userRepository.findById(dtoEntity.getUserId());
        if (user == null) {
            throw new RepositoryException("User has not yet been created.");
        }
        List<Certificate> certificates = certificateIds.stream().map(id -> {
            Certificate certificate = certificateRepository.findById(id);
            if (certificate == null) {
                throw new RepositoryException("Certificate has not yet been created.");
            }
            return certificate;
        }).collect(Collectors.toList());
        BigDecimal cost = certificates.stream().map(Certificate::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
        purchase = new Purchase(LocalDate.now(), cost, user, certificates);
        purchase = purchaseDao.save(purchase);
        return purchaseDtoMapper.toDto(purchase);
    }

    @Override
    public EntityPageDto<PurchaseDto> findAll(Map<String, String> params) {
        params = GeneralParamValidation.validate(params, ParameterValueList.PURCHASE_SORTING_FIELDS.getValues(),
                ParameterValueList.PURCHASE_SEARCHING_FIELDS.getValues());
        if (params.containsKey("USER_ID")) {
            int userId;
            try {
                userId = Integer.parseInt(params.get("USER_ID"));
            } catch (NumberFormatException e) {
                throw new ValidationParamException("Input parameters is not an integer.");
            }
            if (userId <= 0) {
                throw new ValidationParamException("Entity id is zero or not positive integer.");
            }
        }
        EntityCount<Purchase> purchaseEntityCount = purchaseDao.findAll(params);
        List<PurchaseDto> purchases = purchaseEntityCount.getEntities()
                .stream()
                .map(purchaseDtoMapper::toDto)
                .collect(Collectors.toList());
        if (purchases.isEmpty()) {
            throw new EmptyResultException();
        }
        int counts = purchaseEntityCount.getCounts();
        int pageSize = Integer.parseInt(params.get(Parameter.PAGE_SIZE.name()));
        int page = Integer.parseInt(params.get(Parameter.PAGE.name()));
        int totalPages = (int) (Math.ceil((double) counts / pageSize));
        if (totalPages < page) {
            throw new ValidationParamException(Parameter.PAGE.name() + " is more than possible: " + totalPages + ".");
        }
        return new EntityPageDto<>(purchases, counts);
    }

    @Override
    public PurchaseDto findById(Integer id) {
        Purchase purchase = purchaseDao.findById(id);
        if (purchase == null) {
            throw new EmptyResultException();
        }
        return purchaseDtoMapper.toDto(purchase);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (purchaseDao.findById(id) == null) {
            throw new DeleteEntityException("There is no entity with this identifier. There is nothing to delete.");
        }
        purchaseDao.deleteById(id);
    }
}
