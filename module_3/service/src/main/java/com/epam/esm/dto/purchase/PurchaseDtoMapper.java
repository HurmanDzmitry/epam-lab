package com.epam.esm.dto.purchase;

import com.epam.esm.entity.Purchase;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class PurchaseDtoMapper {

    private final ModelMapper mapper;

    @Autowired
    public PurchaseDtoMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Purchase toEntity(PurchaseDto dto) {
        return Objects.isNull(dto) ? null : mapper.map(dto, Purchase.class);
    }

    public PurchaseDto toDto(Purchase entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, PurchaseDto.class);
    }
}

