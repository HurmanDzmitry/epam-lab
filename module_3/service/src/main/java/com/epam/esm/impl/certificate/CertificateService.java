package com.epam.esm.impl.certificate;

import com.epam.esm.ApiService;
import com.epam.esm.dto.certificate.CertificateDto;

import java.util.List;

public interface CertificateService extends ApiService<CertificateDto, Integer> {
}
