package com.epam.esm.dto.certificate;

import com.epam.esm.dto.tag.TagDto;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

@NoArgsConstructor
@Data
@ToString
public class CertificateDto implements Serializable {

    private static final long serialVersionUID = 5489046900254811286L;

    private int id;

    @NotNull(message = "shouldn't be null.")
    @Size(min = 3, max = 300, message = "should be min={min} and max={max}.")
    private String name;

    @NotNull(message = "shouldn't be null.")
    @DecimalMin(value = "0", message = "should be {value} or more.")
    private BigDecimal price;

    @NotNull(message = "shouldn't be null.")
    @Min(value = 1, message = "should be min={value}.")
    @Max(value = 365, message = "should be max={value}.")
    private int duration;

    @NotNull(message = "shouldn't be null.")
    @NotEmpty(message = "shouldn't be empty.")
    private Set<@Valid @NotNull(message = "shouldn't be null.") TagDto> tags;
}
