package com.epam.esm.dto.tag;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.io.Serializable;

@NoArgsConstructor
@Data
@ToString
public class TagDto implements Serializable {

    private static final long serialVersionUID = -3289018326007960344L;

    private int id;

    @NotNull(message = "shouldn't be null.")
    @Size(min = 3, max = 100, message = "should be min={min} and max={max}.")
    private String name;
}