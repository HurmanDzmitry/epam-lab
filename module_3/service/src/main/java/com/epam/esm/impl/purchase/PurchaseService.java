package com.epam.esm.impl.purchase;

import com.epam.esm.ApiService;
import com.epam.esm.dto.purchase.PurchaseDto;

public interface PurchaseService extends ApiService<PurchaseDto, Integer> {
}
