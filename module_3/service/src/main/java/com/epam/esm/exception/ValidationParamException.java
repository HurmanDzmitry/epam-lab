package com.epam.esm.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class ValidationParamException extends RuntimeException {

    private static final long serialVersionUID = 5128349507899742033L;

    public ValidationParamException() {
    }

    public ValidationParamException(String message) {
        super(message);
    }

    public ValidationParamException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationParamException(Throwable cause) {
        super(cause);
    }
}

